# type-graphql-query
Powerful filter decorator supporting and/or operations for [type-graphql](https://typegraphql.com/).

## Usage
For fields that support filter, use `@FilterableField` instead of `@Field`.

```typescript
import { ObjectType, Int } from "type-graphql";
import { FilterableField } from "type-graphql-query";

@ObjectType("Example")
export class ExampleModel {

  @FilterableField()
  myField: string;

  @FilterableField(type => Int)
  numberField: number;

  @FilterableField()
  dateField: Date;
}
```

Then in resolver, use `@FilterArg`.
```typescript
import { Resolver, Query } from "type-graphql";
import { FilterArg } from "type-graphql-query";
import { ExampleModel } from "./models";

@Resolver()
export class ExamplesResolver {
  @Query(returns => String)
  exampleQuery(
    @FilterArg(ExampleModel) filter: any,
  ) {
    // perform operation based on filter argument
    return "hello"
  }
}
```

A recursive filter would be generated in GraphQL schema. Only a few of comparisons are shown here. The comparison operations are borrowed from [nestjs-query](https://doug-martin.github.io/nestjs-query/). Please refer to nestjs-query doc for [a full list of comparison](https://doug-martin.github.io/nestjs-query/docs/concepts/queries#filter-reference).

```graphql
input ExampleFilter {
  myField: StringFieldComparison
  numberField: IntFieldComparison
  dateField: DateFieldComparison
  and: [ExampleFilter!]!
  or: [ExampleFilter!]!
}

input StringFieldComparison {
  eq: String
  in: [String!]
  like: String
  notIn: [String!]
  notLike: String
}

type Query {
  exampleQuery(filter: ExampleFilter!): String!
}
```