import { RelationOptions as TypeOrmRelationOptions } from 'typeorm'

export interface RelationOptions extends TypeOrmRelationOptions {
  filterable?: boolean,
}