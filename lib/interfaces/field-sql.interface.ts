export type FieldSQL<T> = {
  [K in keyof T]?: string;
};