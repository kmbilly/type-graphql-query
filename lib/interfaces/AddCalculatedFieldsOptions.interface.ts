export interface AddCalculatedFieldsOptions {
  // for DateOnly or Timestamp
  age?: string,
  year?: string,
  month?: string,
  yearMonth?: string,
  monthDay?: string,
}