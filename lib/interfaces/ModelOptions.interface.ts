export interface ModelOptions {
  access?: {
    get: string | string[],
    create: string | string[],
    update: string | string[],
    delete: string | string[],
  },
}