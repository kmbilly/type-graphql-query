import { ReturnTypeFunc } from "type-graphql/dist/decorators/types";
import { ColumnOptions } from "typeorm";

export interface DataFieldOptions {
  type?: ReturnTypeFunc,
  required?: boolean,
  length?: number,
  default?: any,
  filterable?: boolean,
  readOnly?: boolean,
  private?: boolean,
  unique?: boolean,
  create?: boolean,
  update?: boolean,
  precision?: number,
  scale?: number,
  columnOptions?: ColumnOptions,
}