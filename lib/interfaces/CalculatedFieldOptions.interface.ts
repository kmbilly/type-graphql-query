import { ReturnTypeFunc } from "type-graphql/dist/decorators/types";

export interface CalculatedFieldOptions {
  type?: ReturnTypeFunc,
  sql?: string,
}