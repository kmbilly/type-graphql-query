import { GraphQLResolveInfo } from "graphql"
import { ColumnMetadata } from "typeorm/metadata/ColumnMetadata"
import { RelationMetadata } from "typeorm/metadata/RelationMetadata"
import { FilterQueryBuilder } from "./filter-query.builder"
import { getMetadataStorage } from "../../metadata"
import { getMetadataStorage as getTypeGraphQLMetadataStorage } from "type-graphql/dist/metadata/getMetadataStorage";
import { getConnection } from '../../helpers/databaseConnection';
import { ListStat } from "../../types"

const graphqlFields = require('graphql-fields')

export const getColumnExpression = (key:string, fieldSQL:any, alias?:string) => {
  let col
  if (fieldSQL && fieldSQL[key]) {
    col = fieldSQL[key]
  } else {
    col = alias ? `${alias}.${key}` : `${key}`
  }

  return col
}

// must use getRawOne or getRawMany to get also any calculated columns
export const applyRequestedFields = (queryBuilder:any, gqlFields:string[], fieldSQL?:any, alias?:string) => {
  const fields = gqlFields
    // .filter((key:string) => key !== '__typename')

  fields.forEach((key:any, index:any) => {
    const col = getColumnExpression(key, fieldSQL, alias)

    if (index === 0) {
      queryBuilder.select(col, key)
    } else {
      queryBuilder.addSelect(col, key)
    }
  })
}

const getFieldArgumentsAsObj:any = (argsList:any) => {
  const argsObj:any = {}
  if (argsList) {
    argsList.forEach((arg:any) => {
      const argName = Object.keys(arg)[0]
      argsObj[argName] = arg[argName].value
    })
  }

  return argsObj
}

// const getPrimaryColumns:any = (metadata:any) => {
//   return metadata.primaryColumns.map((col:any) => col.propertyName)
// }

const getRelationArgs = (baseTypeValue: any, relation:RelationMetadata, realFieldName:string, gqlFields:any) => {
  const inputArgs = (relation.isOneToOne || relation.isManyToOne) ? 
    {} : 
    getFieldArgumentsAsObj(gqlFields[realFieldName].__arguments)
  if (!inputArgs.filter) {
    inputArgs.filter = {}
  }
  if (relation.isOwning) {
    relation.joinColumns.forEach((col:ColumnMetadata) => {
      inputArgs.filter[col.referencedColumn!.propertyName] = { eq: baseTypeValue[col.propertyName] }
    })
  } else {
    relation.inverseRelation!.joinColumns.forEach((col:ColumnMetadata) => {
      inputArgs.filter[col.propertyName] = { eq: baseTypeValue[col.referencedColumn!.propertyName] }
    })
  }

  return inputArgs
}

const getRelationPropertyName = (propertyName:string) => propertyName.startsWith('_') ? propertyName.substr(1) : propertyName;

const autoTypeCastRows = (rawRows:any, modelType:any) => {
  const typeGraphQLMetadata = getTypeGraphQLMetadataStorage();
  const objectTypesList = typeGraphQLMetadata.objectTypes;
  const graphQLModel = objectTypesList.find((ot) => ot.target === modelType);

  if (graphQLModel && graphQLModel.fields) {
    return rawRows.map((rowObj:any) => autoTypeCastRow(rowObj, modelType, graphQLModel.fields))
  }

  return rawRows
}

const autoTypeCastRow = (rawColObj:any, modelType:any, inFieldsInfo?:any):any => {
  let fieldsInfo = inFieldsInfo
  if (!fieldsInfo) {
    const typeGraphQLMetadata = getTypeGraphQLMetadataStorage();
    const objectTypesList = typeGraphQLMetadata.objectTypes;
    const graphQLModel = objectTypesList.find((ot) => ot.target === modelType);
    fieldsInfo = graphQLModel?.fields
  }

  if (fieldsInfo) {
    Object.keys(rawColObj).forEach((key:string) => {
      const fieldInfo = fieldsInfo?.find((model:any) => model.name === key)

      if (typeof rawColObj[key] === 'string') {
        if (fieldInfo?.getType() === Boolean) {
          rawColObj[key] = rawColObj[key] === 'true' || rawColObj[key] === '1' ? true : false
        } else if (fieldInfo?.getType() === Number) {
          rawColObj[key] = Number(rawColObj[key])
        }
      }
    })
  }

  return rawColObj
}

export const getDataByGqlQuery = async (gqlInfo: GraphQLResolveInfo, gqlInput: any, returnType:any) => {
  const gqlFields = graphqlFields(gqlInfo, {}, { excludedFields: ['__typename'], processArguments: true })
  return _getDataByGqlQuery(gqlFields, gqlInput, returnType)
}

const _getDataByGqlQuery = async (gqlFields: any, gqlInput: any, returnType:any, alias?:string) => {
  const { filter, sorting, paging } = gqlInput;
  let result: typeof returnType = {};

  let returnOne = true
  let requestedFields
  let baseType:any = returnType
  if (Array.isArray(returnType)) {
    baseType = returnType[0]
    returnOne = false
    requestedFields = gqlFields
    result = []
  } else {
    requestedFields = gqlFields
  }

  // const typeGraphQLMetadata = getTypeGraphQLMetadataStorage();
  // const objectTypesList = typeGraphQLMetadata.objectTypes;
  // const graphQLModel = objectTypesList.find((ot) => ot.target === baseType);

  const repo = getConnection().getRepository(baseType)

  const filterQueryBuilder = new FilterQueryBuilder<any>(repo)

  // if return array, prepare paging and count value if requested
  const sqlByField = getMetadataStorage().fieldLogic.find(l => l.target === baseType)?.sqlByField
  // if (!returnOne) {
  //   if (gqlFields.pageInfo) {
  //     result.pageInfo = paging
  //   }
  //   if (gqlFields.pageInfo?.totalCount) {
  //     const countAllQueryBuilder = filterQueryBuilder.select(
  //       {
  //         filter,
  //       },
  //       sqlByField
  //     )
  //     const { totalCount } = await countAllQueryBuilder.select('count(1)', 'totalCount').getRawOne()
  //     result.pageInfo.totalCount = totalCount
  //   }
  // }

  // if (returnOne || gqlFields.nodes) {
    const requestedFieldList = Object.keys(requestedFields)
    const relations:RelationMetadata[] = []
    const relationStats:RelationMetadata[] = []
    const relationFieldList:string[] = []
    repo.metadata.relations.forEach((r:RelationMetadata) => {
      const realFieldName = getRelationPropertyName(r.propertyName);
      if (requestedFieldList.includes(realFieldName)) {
        relations.push(r)
        relationFieldList.push(realFieldName)
      }
      if (requestedFieldList.includes(`${realFieldName}Stat`)) {
        relationStats.push(r)
      }
    })

    const columns = repo.metadata.columns.map(c => c.propertyName);
    const calculatedFields = sqlByField ? Object.keys(sqlByField) : [];
    const baseTypeFields:string[] = requestedFieldList.filter(field => columns.includes(field) || calculatedFields.includes(field))

    // get data of base type
    const queryBuilder = filterQueryBuilder.select(
      {
        filter,
        sorting,
        paging
      },
      sqlByField
    )
    const relationJoinProperties:string[] = []
    relations.forEach((relation:RelationMetadata) => {
      Array.prototype.push.apply(
        relationJoinProperties, 
        relation.isOwning ? 
          relation.joinColumns.map(col => col.propertyName) : 
          relation.inverseRelation!.joinColumns.map(col => col.referencedColumn!.propertyName)
      )
    })
    relationJoinProperties.forEach((joinProp:string) => {
      if (!baseTypeFields.includes(joinProp)) {
        baseTypeFields.push(joinProp)
      }
    })
    applyRequestedFields(queryBuilder, baseTypeFields, sqlByField, alias || baseType.name)

    if (returnOne) {
      result = await queryBuilder.getRawOne()
      if (result) {
        result = autoTypeCastRow(result, baseType)
      }

      // get data of any requested relations
      relations.forEach((relation:RelationMetadata) => {
        if (relation) {
          const realFieldName = getRelationPropertyName(relation.propertyName);
          result[realFieldName] = _getDataByGqlQuery(
            gqlFields[realFieldName],
            getRelationArgs(result, relation, realFieldName, gqlFields),
            relation.isOneToMany || relation.isManyToMany ? [relation.type] : relation.type
          )
        }
      })
      relationStats.forEach((relationStat:RelationMetadata) => {
        if (relationStat) {
          const realFieldName = getRelationPropertyName(relationStat.propertyName);
          const statFieldName = `${realFieldName}Stat`;
          result[statFieldName] = _getStatByGqlQuery(
            gqlFields[statFieldName],
            getRelationArgs(result, relationStat, statFieldName, gqlFields),
            relationStat.type
          )
        }
      })
    } else {
      result = await queryBuilder.getRawMany()
      result = autoTypeCastRows(result, baseType)
      result.forEach((node:any) => {

        // get data of any requested relations
        relations.forEach((relation:RelationMetadata) => {
          if (relation) {
            const realFieldName = getRelationPropertyName(relation.propertyName);
            node[realFieldName] = _getDataByGqlQuery(
              gqlFields[realFieldName],
              getRelationArgs(node, relation, realFieldName, gqlFields),
              relation.isOneToMany || relation.isManyToMany ? [relation.type] : relation.type
            )
          }
        })
        relationStats.forEach((relationStat:RelationMetadata) => {
          if (relationStat) {
            const realFieldName = getRelationPropertyName(relationStat.propertyName);
            const statFieldName = `${realFieldName}Stat`;
            node[statFieldName] = _getStatByGqlQuery(
              gqlFields[statFieldName],
              getRelationArgs(node, relationStat, statFieldName, gqlFields),
              relationStat.type
            )
          }
        })          
      })
    }

  // }

  return result
}

const _getStatByGqlQuery = async (gqlFields: any, gqlInput: any, returnType:any):Promise<ListStat> => {
  const { filter } = gqlInput;
  let result: ListStat = {};

  let requestedFields = gqlFields

  const repo = getConnection().getRepository(returnType)

  const filterQueryBuilder = new FilterQueryBuilder<any>(repo)

  // if return array, prepare paging and count value if requested
  const sqlByField = getMetadataStorage().fieldLogic.find(l => l.target === returnType)?.sqlByField
  if (requestedFields.totalCount) {
    const countAllQueryBuilder = filterQueryBuilder.select(
      {
        filter,
      },
      sqlByField
    )
    const { totalCount } = await countAllQueryBuilder.select('count(1)', 'totalCount').getRawOne()
    result.totalCount = totalCount
  }

  return result
}

export const getStatByGqlQuery = async (gqlInfo: GraphQLResolveInfo, gqlInput: any, returnType:any) => {
  const gqlFields = graphqlFields(gqlInfo, {}, { excludedFields: ['__typename'], processArguments: true })
  return _getStatByGqlQuery(gqlFields, gqlInput, returnType)
}
