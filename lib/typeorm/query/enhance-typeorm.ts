import { SelectQueryBuilder, UpdateQueryBuilder, DeleteQueryBuilder } from 'typeorm';
import { SoftDeleteQueryBuilder } from 'typeorm/query-builder/SoftDeleteQueryBuilder';

declare module 'typeorm' {
  interface WhereExpression {
    // whereNot(query: Brackets): this;
    // andWhereNot(query: Brackets): this;
    // orWhereNot(query: Brackets): this;
    whereExists(query: WhereExpression): this;
    andWhereExists(query: WhereExpression): this;
    orWhereExists(query: WhereExpression): this;
    whereNotExists(query: WhereExpression): this;
    andWhereNotExists(query: WhereExpression): this;
    orWhereNotExists(query: WhereExpression): this;
  }

  interface SelectQueryBuilder<Entity> {
    // whereNot<T>(query: Brackets): this;
    // andWhereNot<T>(query: Brackets): this;
    // orWhereNot<T>(query: Brackets): this;
    whereExists<T>(query: SelectQueryBuilder<T>): this;
    andWhereExists<T>(query: SelectQueryBuilder<T>): this;
    orWhereExists<T>(query: SelectQueryBuilder<T>): this;
    whereNotExists<T>(query: SelectQueryBuilder<T>): this;
    andWhereNotExists<T>(query: SelectQueryBuilder<T>): this;
    orWhereNotExists<T>(query: SelectQueryBuilder<T>): this;
  }

  interface UpdateQueryBuilder<Entity> {
    // whereNot<T>(query: Brackets): this;
    // andWhereNot<T>(query: Brackets): this;
    // orWhereNot<T>(query: Brackets): this;
    whereExists<T>(query: UpdateQueryBuilder<T>): this;
    andWhereExists<T>(query: UpdateQueryBuilder<T>): this;
    orWhereExists<T>(query: UpdateQueryBuilder<T>): this;
    whereNotExists<T>(query: UpdateQueryBuilder<T>): this;
    andWhereNotExists<T>(query: UpdateQueryBuilder<T>): this;
    orWhereNotExists<T>(query: UpdateQueryBuilder<T>): this;
  }

  interface DeleteQueryBuilder<Entity> {
    // whereNot<T>(query: Brackets): this;
    // andWhereNot<T>(query: Brackets): this;
    // orWhereNot<T>(query: Brackets): this;
    whereExists<T>(query: DeleteQueryBuilder<T>): this;
    andWhereExists<T>(query: DeleteQueryBuilder<T>): this;
    orWhereExists<T>(query: DeleteQueryBuilder<T>): this;
    whereNotExists<T>(query: DeleteQueryBuilder<T>): this;
    andWhereNotExists<T>(query: DeleteQueryBuilder<T>): this;
    orWhereNotExists<T>(query: DeleteQueryBuilder<T>): this;
  }
}

declare module 'typeorm/query-builder/SoftDeleteQueryBuilder' {
  interface SoftDeleteQueryBuilder<Entity> {
    // whereNot<T>(query: Brackets): this;
    // andWhereNot<T>(query: Brackets): this;
    // orWhereNot<T>(query: Brackets): this;
    whereExists<T>(query: SoftDeleteQueryBuilder<T>): this;
    andWhereExists<T>(query: SoftDeleteQueryBuilder<T>): this;
    orWhereExists<T>(query: SoftDeleteQueryBuilder<T>): this;
    whereNotExists<T>(query: SoftDeleteQueryBuilder<T>): this;
    andWhereNotExists<T>(query: SoftDeleteQueryBuilder<T>): this;
    orWhereNotExists<T>(query: SoftDeleteQueryBuilder<T>): this;
  }
}

// const getWhereSql = <Entity>(qb:QueryBuilder<Entity>) => {
//   const sql = qb.getQuery();
//   const whereSql = sql.substr(sql.indexOf('WHERE') + 5);
//   return whereSql;
// }

// TODO how to implement not (filter expression) ?????
// SelectQueryBuilder.prototype.whereNot = function (brackets: Brackets): SelectQueryBuilder<any> {
//   // const whereQueryBuilder = this.createQueryBuilder();
//   // whereQueryBuilder.expressionMap.mainAlias = this.expressionMap.mainAlias;
//   // whereQueryBuilder.expressionMap.aliasNamePrefixingEnabled = this.expressionMap.aliasNamePrefixingEnabled;
//   // whereQueryBuilder.expressionMap.nativeParameters = this.expressionMap.nativeParameters;
//   const whereQueryBuilder = this.subQuery();
//   whereQueryBuilder.select('1');
//   brackets.whereFactory(whereQueryBuilder);
//   return this.where(`NOT ${getWhereSql(whereQueryBuilder)}`, whereQueryBuilder.getParameters());
// };

// SelectQueryBuilder.prototype.andWhereNot = function (brackets: Brackets): SelectQueryBuilder<any> {
//   // const whereQueryBuilder = this.createQueryBuilder();
//   // whereQueryBuilder.expressionMap.mainAlias = this.expressionMap.mainAlias;
//   // whereQueryBuilder.expressionMap.aliasNamePrefixingEnabled = this.expressionMap.aliasNamePrefixingEnabled;
//   // whereQueryBuilder.expressionMap.nativeParameters = this.expressionMap.nativeParameters;
//   console.log('andWhereNot');
//   console.log(this.expressionMap);
//   const whereQueryBuilder = this.subQuery();
//   whereQueryBuilder.select('1');
//   brackets.whereFactory(whereQueryBuilder);
//   return this.andWhere(`NOT ${getWhereSql(whereQueryBuilder)}`, whereQueryBuilder.getParameters());
// };

// SelectQueryBuilder.prototype.orWhereNot = function (brackets: Brackets): SelectQueryBuilder<any> {
//   // const whereQueryBuilder = this.createQueryBuilder();
//   // whereQueryBuilder.expressionMap.mainAlias = this.expressionMap.mainAlias;
//   // whereQueryBuilder.expressionMap.aliasNamePrefixingEnabled = this.expressionMap.aliasNamePrefixingEnabled;
//   // whereQueryBuilder.expressionMap.nativeParameters = this.expressionMap.nativeParameters;
//   const whereQueryBuilder = this.subQuery();
//   whereQueryBuilder.select('1');
//   brackets.whereFactory(whereQueryBuilder);
//   return this.orWhere(`NOT ${getWhereSql(whereQueryBuilder)}`, whereQueryBuilder.getParameters());
// };

SelectQueryBuilder.prototype.whereExists = function (query: SelectQueryBuilder<any>): SelectQueryBuilder<any> {
  return this.where(`EXISTS (${query.getQuery()})`, query.getParameters());
};

SelectQueryBuilder.prototype.andWhereExists = function (query: SelectQueryBuilder<any>): SelectQueryBuilder<any> {
  return this.andWhere(`EXISTS (${query.getQuery()})`, query.getParameters());
};

SelectQueryBuilder.prototype.orWhereExists = function (query: SelectQueryBuilder<any>): SelectQueryBuilder<any> {
  return this.orWhere(`EXISTS (${query.getQuery()})`, query.getParameters());
};

SelectQueryBuilder.prototype.whereNotExists = function (query: SelectQueryBuilder<any>): SelectQueryBuilder<any> {
  return this.where(`NOT EXISTS (${query.getQuery()})`, query.getParameters());
};

SelectQueryBuilder.prototype.andWhereNotExists = function (query: SelectQueryBuilder<any>): SelectQueryBuilder<any> {
  return this.andWhere(`NOT EXISTS (${query.getQuery()})`, query.getParameters());
};

SelectQueryBuilder.prototype.orWhereNotExists = function (query: SelectQueryBuilder<any>): SelectQueryBuilder<any> {
  return this.orWhere(`NOT EXISTS (${query.getQuery()})`, query.getParameters());
};

// UpdateQueryBuilder.prototype.whereNot = function (brackets: Brackets): UpdateQueryBuilder<any> {
//   const whereQueryBuilder = this.createQueryBuilder();
//   whereQueryBuilder.select('1');
//   brackets.whereFactory(whereQueryBuilder);
//   return this.where(`NOT (${getWhereSql(whereQueryBuilder)})`, whereQueryBuilder.getParameters());
// };

// UpdateQueryBuilder.prototype.andWhereNot = function (brackets: Brackets): UpdateQueryBuilder<any> {
//   const whereQueryBuilder = this.createQueryBuilder();
//   whereQueryBuilder.select('1');
//   brackets.whereFactory(whereQueryBuilder);
//   return this.andWhere(`NOT (${getWhereSql(whereQueryBuilder)})`, whereQueryBuilder.getParameters());
// };

// UpdateQueryBuilder.prototype.orWhereNot = function (brackets: Brackets): UpdateQueryBuilder<any> {
//   const whereQueryBuilder = this.createQueryBuilder();
//   whereQueryBuilder.select('1');
//   brackets.whereFactory(whereQueryBuilder);
//   return this.orWhere(`NOT (${getWhereSql(whereQueryBuilder)})`, whereQueryBuilder.getParameters());
// };

UpdateQueryBuilder.prototype.whereExists = function (query: UpdateQueryBuilder<any>): UpdateQueryBuilder<any> {
  return this.where(`EXISTS (${query.getQuery()})`, query.getParameters());
};

UpdateQueryBuilder.prototype.andWhereExists = function (query: UpdateQueryBuilder<any>): UpdateQueryBuilder<any> {
  return this.andWhere(`EXISTS (${query.getQuery()})`, query.getParameters());
};

UpdateQueryBuilder.prototype.orWhereExists = function (query: UpdateQueryBuilder<any>): UpdateQueryBuilder<any> {
  return this.orWhere(`EXISTS (${query.getQuery()})`, query.getParameters());
};

UpdateQueryBuilder.prototype.whereNotExists = function (query: UpdateQueryBuilder<any>): UpdateQueryBuilder<any> {
  return this.where(`NOT EXISTS (${query.getQuery()})`, query.getParameters());
};

UpdateQueryBuilder.prototype.andWhereNotExists = function (query: UpdateQueryBuilder<any>): UpdateQueryBuilder<any> {
  return this.andWhere(`NOT EXISTS (${query.getQuery()})`, query.getParameters());
};

UpdateQueryBuilder.prototype.orWhereNotExists = function (query: UpdateQueryBuilder<any>): UpdateQueryBuilder<any> {
  return this.orWhere(`NOT EXISTS (${query.getQuery()})`, query.getParameters());
};

// DeleteQueryBuilder.prototype.whereNot= function (brackets: Brackets): DeleteQueryBuilder<any> {
//   const whereQueryBuilder = this.createQueryBuilder();
//   whereQueryBuilder.select('1');
//   brackets.whereFactory(whereQueryBuilder);
//   return this.where(`NOT (${getWhereSql(whereQueryBuilder)})`, whereQueryBuilder.getParameters());
// };

// DeleteQueryBuilder.prototype.andWhereNot = function (brackets: Brackets): DeleteQueryBuilder<any> {
//   const whereQueryBuilder = this.createQueryBuilder();
//   whereQueryBuilder.select('1');
//   brackets.whereFactory(whereQueryBuilder);
//   return this.andWhere(`NOT (${getWhereSql(whereQueryBuilder)})`, whereQueryBuilder.getParameters());
// };

// DeleteQueryBuilder.prototype.orWhereNot = function (brackets: Brackets): DeleteQueryBuilder<any> {
//   const whereQueryBuilder = this.createQueryBuilder();
//   whereQueryBuilder.select('1');
//   brackets.whereFactory(whereQueryBuilder);
//   return this.orWhere(`NOT (${getWhereSql(whereQueryBuilder)})`, whereQueryBuilder.getParameters());
// };

DeleteQueryBuilder.prototype.whereExists = function (query: DeleteQueryBuilder<any>): DeleteQueryBuilder<any> {
  return this.where(`EXISTS (${query.getQuery()})`, query.getParameters());
};

DeleteQueryBuilder.prototype.andWhereExists = function (query: DeleteQueryBuilder<any>): DeleteQueryBuilder<any> {
  return this.andWhere(`EXISTS (${query.getQuery()})`, query.getParameters());
};

DeleteQueryBuilder.prototype.orWhereExists = function (query: DeleteQueryBuilder<any>): DeleteQueryBuilder<any> {
  return this.orWhere(`EXISTS (${query.getQuery()})`, query.getParameters());
};

DeleteQueryBuilder.prototype.whereNotExists = function (query: DeleteQueryBuilder<any>): DeleteQueryBuilder<any> {
  return this.where(`NOT EXISTS (${query.getQuery()})`, query.getParameters());
};

DeleteQueryBuilder.prototype.andWhereNotExists = function (query: DeleteQueryBuilder<any>): DeleteQueryBuilder<any> {
  return this.andWhere(`NOT EXISTS (${query.getQuery()})`, query.getParameters());
};

DeleteQueryBuilder.prototype.orWhereNotExists = function (query: DeleteQueryBuilder<any>): DeleteQueryBuilder<any> {
  return this.orWhere(`NOT EXISTS (${query.getQuery()})`, query.getParameters());
};

// SoftDeleteQueryBuilder.prototype.whereNot = function (brackets: Brackets): SoftDeleteQueryBuilder<any> {
//   const whereQueryBuilder = this.createQueryBuilder();
//   whereQueryBuilder.select('1');
//   brackets.whereFactory(whereQueryBuilder);
//   return this.where(`NOT (${getWhereSql(whereQueryBuilder)})`, whereQueryBuilder.getParameters());
// };

// SoftDeleteQueryBuilder.prototype.andWhereNot = function (brackets: Brackets): SoftDeleteQueryBuilder<any> {
//   const whereQueryBuilder = this.createQueryBuilder();
//   whereQueryBuilder.select('1');
//   brackets.whereFactory(whereQueryBuilder);
//   return this.andWhere(`NOT (${getWhereSql(whereQueryBuilder)})`, whereQueryBuilder.getParameters());
// };

// SoftDeleteQueryBuilder.prototype.orWhereNot = function (brackets: Brackets): SoftDeleteQueryBuilder<any> {
//   const whereQueryBuilder = this.createQueryBuilder();
//   whereQueryBuilder.select('1');
//   brackets.whereFactory(whereQueryBuilder);
//   return this.orWhere(`NOT (${getWhereSql(whereQueryBuilder)})`, whereQueryBuilder.getParameters());
// };

SoftDeleteQueryBuilder.prototype.whereExists = function (query: SoftDeleteQueryBuilder<any>): SoftDeleteQueryBuilder<any> {
  return this.where(`EXISTS (${query.getQuery()})`, query.getParameters());
};

SoftDeleteQueryBuilder.prototype.andWhereExists = function (query: SoftDeleteQueryBuilder<any>): SoftDeleteQueryBuilder<any> {
  return this.andWhere(`EXISTS (${query.getQuery()})`, query.getParameters());
};

SoftDeleteQueryBuilder.prototype.orWhereExists = function (query: SoftDeleteQueryBuilder<any>): SoftDeleteQueryBuilder<any> {
  return this.orWhere(`EXISTS (${query.getQuery()})`, query.getParameters());
};

SoftDeleteQueryBuilder.prototype.whereNotExists = function (query: SoftDeleteQueryBuilder<any>): SoftDeleteQueryBuilder<any> {
  return this.where(`NOT EXISTS (${query.getQuery()})`, query.getParameters());
};

SoftDeleteQueryBuilder.prototype.andWhereNotExists = function (query: SoftDeleteQueryBuilder<any>): SoftDeleteQueryBuilder<any> {
  return this.andWhere(`NOT EXISTS (${query.getQuery()})`, query.getParameters());
};

SoftDeleteQueryBuilder.prototype.orWhereNotExists = function (query: SoftDeleteQueryBuilder<any>): SoftDeleteQueryBuilder<any> {
  return this.orWhere(`NOT EXISTS (${query.getQuery()})`, query.getParameters());
};
