import { Brackets, SelectQueryBuilder, WhereExpression } from 'typeorm';
import { getConnection } from '../../helpers/databaseConnection';
import { Filter, FilterComparisonOperators, FilterComparisons, FilterFieldComparison, FieldSQL } from '../../interfaces';
import { getMetadataStorage } from '../../metadata';
import { FilterQueryBuilder, RelationInfos } from './filter-query.builder';
import { EntityComparisonField, SQLComparisonBuilder } from './sql-comparison.builder';

let subQueryCount = 0

const getWhereSql = <Entity>(qb:SelectQueryBuilder<Entity>) => {
  const sql = qb.getQuery();
  const whereSql = sql.substr(sql.indexOf('WHERE') + 5);
  return whereSql;
}

/**
 * @internal
 * Builds a WHERE clause from a Filter.
 */
export class WhereBuilder<Entity> {
  constructor(readonly sqlComparisonBuilder: SQLComparisonBuilder<Entity> = new SQLComparisonBuilder<Entity>()) {}

  // createQueryBuilder: () => SelectQueryBuilder<Entity>

  // setQueryBuilderFactory(factory: () => SelectQueryBuilder<Entity>):WhereBuilder<Entity> {
  //   this.createQueryBuilder = factory;
  //   return this;
  // }

  /**
   * Builds a WHERE clause from a Filter.
   * @param where - the `typeorm` WhereExpression
   * @param filter - the filter to build the WHERE clause from.
   * @param alias - optional alias to use to qualify an identifier
   */
  build<Where extends WhereExpression>(
    where: Where,
    filter: Filter<Entity>,
    entity: Function | string,
    relationNames: RelationInfos[],
    alias?: string,
    fieldSQL?: FieldSQL<Entity>,
  ): Where {
    const { and, or, not } = filter;
    if (and && and.length) {
      this.filterAnd(where, and, entity, relationNames, alias, fieldSQL);
    }
    if (or && or.length) {
      this.filterOr(where, or, entity, relationNames, alias, fieldSQL);
    }
    if (not) {
      this.filterNot(where, not, entity, relationNames, alias, fieldSQL);
    }
    return this.filterFields(where, filter, relationNames, alias, fieldSQL);
  }

  /**
   * ANDs multiple filters together. This will properly group every clause to ensure proper precedence.
   *
   * @param where - the `typeorm` WhereExpression
   * @param filters - the array of filters to AND together
   * @param alias - optional alias to use to qualify an identifier
   */
  private filterAnd<Where extends WhereExpression>(
    where: Where,
    filters: Filter<Entity>[],
    entity: Function | string,
    relationNames: RelationInfos[],
    alias?: string,
    fieldSQL?: FieldSQL<Entity>,
  ): Where {
    return filters.reduce((w, f) => w.andWhere(this.createBrackets(f, entity, relationNames, alias, fieldSQL)), where);
  }

  /**
   * ORs multiple filters together. This will properly group every clause to ensure proper precedence.
   *
   * @param where - the `typeorm` WhereExpression
   * @param filter - the array of filters to OR together
   * @param alias - optional alias to use to qualify an identifier
   */
  private filterOr<Where extends WhereExpression>(
    where: Where,
    filter: Filter<Entity>[],
    entity: Function | string,
    relationNames: RelationInfos[],
    alias?: string,
    fieldSQL?: FieldSQL<Entity>,
  ): Where {
    return filter.reduce((w, f) => where.orWhere(this.createBrackets(f, entity, relationNames, alias, fieldSQL)), where);
  }

  private filterNot<Where extends WhereExpression>(
    where: Where,
    filter: Filter<Entity>,
    entity: Function | string,
    relationNames: RelationInfos[],
    alias?: string,
    fieldSQL?: FieldSQL<Entity>,
  ): Where {
    const newQb = getConnection().getRepository<Entity>(entity).createQueryBuilder();
    (new WhereBuilder<Entity>(new SQLComparisonBuilder<Entity>().setQueryId('' + (subQueryCount++)))).build(newQb, filter, entity, relationNames, alias, fieldSQL);
    newQb.select('1');
    const result = where.andWhere(`NOT (${getWhereSql(newQb)})`, newQb.getParameters());
    return result;
  }

  /**
   * Wraps a filter in brackes to ensure precedence.
   * ```
   * {a: { eq: 1 } } // "(a = 1)"
   * {a: { eq: 1 }, b: { gt: 2 } } // "((a = 1) AND (b > 2))"
   * ```
   * @param filter - the filter to wrap in brackets.
   * @param alias - optional alias to use to qualify an identifier
   */
  private createBrackets(filter: Filter<Entity>, entity: Function | string, relationNames: RelationInfos[], alias?: string, fieldSQL?: FieldSQL<Entity>): Brackets {
    return new Brackets((qb) => this.build(qb, filter, entity, relationNames, alias, fieldSQL));
  }

  /**
   * Creates field comparisons from a filter. This method will ignore and/or properties.
   * @param where - the `typeorm` WhereExpression
   * @param filter - the filter with fields to create comparisons for.
   * @param alias - optional alias to use to qualify an identifier
   */
  private filterFields<Where extends WhereExpression>(
    where: Where,
    filter: Filter<Entity>,
    relationNames: RelationInfos[],
    alias?: string,
    fieldSQL?: FieldSQL<Entity>,
  ): Where {
    return Object.keys(filter).reduce((w, field) => {
      if (field !== 'and' && field !== 'or' && field !== 'not') {
        return this.withFilterComparison(
          where,
          field as keyof Entity,
          this.getField(filter, field as keyof Entity),
          relationNames,
          alias,
          fieldSQL,
        );
      }
      return w;
    }, where);
  }

  private getField<K extends keyof FilterComparisons<Entity>>(
    obj: FilterComparisons<Entity>,
    field: K,
  ): FilterFieldComparison<Entity[K]> {
    return obj[field] as FilterFieldComparison<Entity[K]>;
  }

  private withFilterComparison<T extends keyof Entity, Where extends WhereExpression>(
    where: Where,
    field: T,
    cmp: FilterFieldComparison<Entity[T]>,
    relationNames: RelationInfos[],
    alias?: string,
    fieldSQL?: FieldSQL<Entity>,
  ): Where {
    const relation = relationNames.find((r) => r.name === field as string)
    if (relation) {
      return this.withRelationFilter(where, field, cmp, relation, alias);
    }
    return where.andWhere(
      new Brackets((qb) => {
        const opts = Object.keys(cmp) as FilterComparisonOperators<Entity[T]>[];
        const sqlComparisons = opts.map((cmpType) =>
          this.sqlComparisonBuilder.build(field, cmpType, cmp[cmpType] as EntityComparisonField<Entity, T>, alias, fieldSQL),
        );
        sqlComparisons.map(({ sql, params }) => qb.orWhere(sql, params));
      }),
    );
  }

  private withRelationFilter<T extends keyof Entity, Where extends WhereExpression>(
    where: Where,
    field: T,
    cmp: FilterFieldComparison<Entity[T]>,
    relationInfo: RelationInfos,
    alias?: string,
  ): Where {
    const fieldSQL = getMetadataStorage().fieldLogic.find(l => l.target === relationInfo.type)?.sqlByField

    if (relationInfo.relationType === 'one') {
      return where.andWhere(
        new Brackets((qb) => {
          const relationWhere = new WhereBuilder<Entity[T]>(new SQLComparisonBuilder<Entity[T]>().setQueryId('' + (subQueryCount++)));
          // for now ignore relations of relations.
          return relationWhere.build(qb, cmp as Filter<Entity[T]>, relationInfo.type, [], field as string, fieldSQL);
        }),
      );
    } else {
      const opts = Object.keys(cmp) as FilterComparisonOperators<Entity[T]>[];

      return where.andWhere(
        new Brackets((qb) => {
          opts.forEach((cmpType) => {
            const normalizedCmpType = (cmpType as string).toLowerCase();
            if (normalizedCmpType === 'isNull') {
              const { sql, params } = this.sqlComparisonBuilder.build(field, cmpType, cmp[cmpType] as EntityComparisonField<Entity, T>, alias, fieldSQL);
              qb.orWhere(sql, params);
            } else if (normalizedCmpType === 'some') {
              const newQb = new FilterQueryBuilder(getConnection().getRepository(relationInfo.type), new WhereBuilder<Entity>(new SQLComparisonBuilder<Entity>().setQueryId('' + (subQueryCount++))))
                .select({
                  filter: cmp[cmpType] as Filter<Entity[T]>
                })

              relationInfo.joinProperties!.forEach(joinProp => {
                newQb.andWhere(`${newQb.alias}.${joinProp.from} = ${alias}.${joinProp.to}`)
              })
              newQb.select('1')

              qb.orWhereExists(newQb);
                
                //new WhereBuilder<Entity[T]>().build(qb, cmp[cmpType] as Filter<Entity[T]>, [], field as string)
              // );
            } else if (normalizedCmpType === 'none') {
              const newQb = new FilterQueryBuilder(getConnection().getRepository(relationInfo.type), new WhereBuilder<Entity>(new SQLComparisonBuilder<Entity>().setQueryId('' + (subQueryCount++))))
                .select({
                  filter: cmp[cmpType] as Filter<Entity[T]>
                })

              relationInfo.joinProperties!.forEach(joinProp => {
                newQb.andWhere(`${newQb.alias}.${joinProp.from} = ${alias}.${joinProp.to}`)
              })
              newQb.select('1')

              qb.orWhereNotExists(newQb);
              // qb = qb.orWhere('NOT EXISTS (SELECT 1 FROM `person_to_people_group` `PersonToPeopleGroup` WHERE (`PersonToPeopleGroup`.`role` = :role) AND `PersonToPeopleGroup`.`personId` = Person.id)', { role: 'test'});
            } else if (normalizedCmpType === 'every') {
              const newQb = new FilterQueryBuilder(getConnection().getRepository(relationInfo.type), new WhereBuilder<Entity>(new SQLComparisonBuilder<Entity>().setQueryId('' + (subQueryCount++))))
                .select({
                  filter: {
                    not: cmp[cmpType] as Filter<Entity[T]>
                  }
                })

              relationInfo.joinProperties!.forEach(joinProp => {
                newQb.andWhere(`${newQb.alias}.${joinProp.from} = ${alias}.${joinProp.to}`)
              })
              newQb.select('1')

              qb.orWhereNotExists(newQb);
            }
          });
        }),    
      )
    }
  }
}
