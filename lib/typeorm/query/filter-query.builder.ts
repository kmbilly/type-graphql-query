import { Filter, Paging, Query, FieldSQL, SortField } from '../../interfaces';
import { getFilterFields } from '../../helpers'
import {
  DeleteQueryBuilder,
  QueryBuilder,
  Repository,
  SelectQueryBuilder,
  UpdateQueryBuilder,
  WhereExpression,
} from 'typeorm';
import { SoftDeleteQueryBuilder } from 'typeorm/query-builder/SoftDeleteQueryBuilder';
import { WhereBuilder } from './where.builder';
import { ColumnMetadata } from 'typeorm/metadata/ColumnMetadata';
import { getSortFields } from '../../helpers/getSortFields';
import { getMetadataStorage } from '../../metadata';

/**
 * @internal
 *
 * Interface that for Typeorm query builders that are sortable.
 */
interface Sortable<Entity> extends QueryBuilder<Entity> {
  addOrderBy(sort: string, order?: 'ASC' | 'DESC', nulls?: 'NULLS FIRST' | 'NULLS LAST'): this;
}

/**
 * @internal
 *
 * Interface for `typeorm` query builders that are pageable.
 */
interface Pageable<Entity> extends QueryBuilder<Entity> {
  limit(limit?: number): this;
  offset(offset?: number): this;
}

export interface RelationInfos {
  relationType: 'one' | 'many',
  name: string,
  type: Function | string,
  joinProperties?: {
    from: string,
    to: string,
  }[],
}

/**
 * @internal
 *
 * Class that will convert a Query into a `typeorm` Query Builder.
 */
export class FilterQueryBuilder<Entity> {
  constructor(
    readonly repo: Repository<Entity>,
    readonly whereBuilder: WhereBuilder<Entity> = new WhereBuilder<Entity>(),
  ) {}

  /**
   * Create a `typeorm` SelectQueryBuilder with `WHERE`, `ORDER BY` and `LIMIT/OFFSET` clauses.
   *
   * @param query - the query to apply.
   */
  select(query: Query<Entity>, fieldSQL?: FieldSQL<Entity>): SelectQueryBuilder<Entity> {
    let qb = this.createQueryBuilder();
    qb = this.applyRelationJoins(qb, query.filter, query.sorting);
    qb = this.applyFilter(qb, query.filter, qb.alias, fieldSQL);
    qb = this.applySorting(qb, query.sorting, qb.alias, fieldSQL);
    qb = this.applyPaging(qb, query.paging);
    return qb;
  }

  /**
   * Create a `typeorm` DeleteQueryBuilder with a WHERE clause.
   *
   * @param query - the query to apply.
   */
  delete(query: Query<Entity>): DeleteQueryBuilder<Entity> {
    return this.applyFilter(this.repo.createQueryBuilder().delete(), query.filter);
  }

  /**
   * Create a `typeorm` DeleteQueryBuilder with a WHERE clause.
   *
   * @param query - the query to apply.
   */
  softDelete(query: Query<Entity>): SoftDeleteQueryBuilder<Entity> {
    return this.applyFilter(
      this.repo.createQueryBuilder().softDelete() as SoftDeleteQueryBuilder<Entity>,
      query.filter,
    );
  }

  /**
   * Create a `typeorm` UpdateQueryBuilder with `WHERE` and `ORDER BY` clauses
   *
   * @param query - the query to apply.
   */
  update(query: Query<Entity>): UpdateQueryBuilder<Entity> {
    const qb = this.applyFilter(this.repo.createQueryBuilder().update(), query.filter);
    return this.applySorting(qb, query.sorting);
  }

  /**
   * Applies paging to a Pageable `typeorm` query builder
   * @param qb - the `typeorm` QueryBuilder
   * @param paging - the Paging options.
   */
  applyPaging<P extends Pageable<Entity>>(qb: P, paging?: Paging): P {
    if (!paging) {
      return qb;
    }
    return qb.limit(paging.limit).offset(paging.offset);
  }

  /**
   * Applies the filter from a Query to a `typeorm` QueryBuilder.
   *
   * @param qb - the `typeorm` QueryBuilder.
   * @param filter - the filter.
   * @param alias - optional alias to use to qualify an identifier
   */
  applyFilter<Where extends WhereExpression>(qb: Where, filter?: Filter<Entity>, alias?: string, fieldSQL?: FieldSQL<Entity>): Where {
    if (!filter) {
      return qb;
    }
    return this.whereBuilder.build(qb, filter, this.repo.target as string, this.getReferencedRelations(filter), alias, fieldSQL);
  }

  /**
   * Applies the ORDER BY clause to a `typeorm` QueryBuilder.
   * @param qb - the `typeorm` QueryBuilder.
   * @param sorts - an array of SortFields to create the ORDER BY clause.
   * @param alias - optional alias to use to qualify an identifier
   */
  applySorting<T extends Sortable<Entity>>(qb: T, sorts?: SortField<Entity>[], alias?: string, fieldSQL?: FieldSQL<Entity>): T {
    if (!sorts) {
      return qb;
    }

    return sorts.reduce((prevQb, { field, direction, nulls }) => {
      let col;
      const fieldParts = (field as string).split('.')
      if (fieldParts.length > 1) {
        const baseName = (field as string).split('.')[0];
        const relationInfo = this.relationNames.find((r:any) => r.relationType === 'one' && r.name === baseName);
        if (relationInfo) {
          const relationFieldSQL = getMetadataStorage().fieldLogic.find(l => l.target === relationInfo.type)?.sqlByField
          if (relationFieldSQL && relationFieldSQL[fieldParts[1]]) {
            col = relationFieldSQL[fieldParts[1]]!
          } else {
            col = `${field as string}`;
          }
        }
      }

      if (!col) {
        col = alias ? `${alias}.${field as string}` : `${field as string}`;
        if (fieldSQL && fieldSQL[field]) {
          col = fieldSQL[field]!
        }
      }

      return prevQb.addOrderBy(col, direction, nulls);
    }, qb);
  }

  /**
   * Create a `typeorm` SelectQueryBuilder which can be used as an entry point to create update, delete or insert
   * QueryBuilders.
   */
  private createQueryBuilder(): SelectQueryBuilder<Entity> {
    return this.repo.createQueryBuilder();
  }

  private applyRelationJoins(qb: SelectQueryBuilder<Entity>, filter?: Filter<Entity>, sorts?: SortField<Entity>[]): SelectQueryBuilder<Entity> {
    if (!filter && !sorts) {
      return qb;
    }
    const referencedRelations = this.getReferencedRelations(filter, sorts);
    return referencedRelations.filter(relation => relation.relationType === 'one').reduce((rqb, relation) => {
      return rqb.leftJoin(`${rqb.alias}.${relation.name}`, relation.name);
    }, qb);
  }

  private getReferencedRelations(filter?: Filter<Entity>, sorts?: SortField<Entity>[]): RelationInfos[] {
    // const { relationNames } = this;
    let referencedFields:string[] = []
    if (filter) {
      referencedFields = referencedFields.concat(getFilterFields(filter));
    }
    if (sorts) {
      referencedFields = referencedFields.concat(getSortFields(sorts));
    }
    return this.relationNames.filter((r:any) => referencedFields.includes(r.name));
  }

  private get relationNames(): RelationInfos[] {
    return this.repo.metadata.relations.map((r) => {
      const relationType = (r.isOneToOne || r.isManyToOne) ? 'one' : 'many';
      
      const info:RelationInfos = {
        relationType,
        name: r.propertyName.startsWith('_') ? r.propertyName.substr(1) : r.propertyName,
        type: r.type,
      }

      if (relationType === 'many') {
        if (r.isOwning) {
          info.joinProperties = r.joinColumns.map((col:ColumnMetadata) => ({
            from: col.referencedColumn!.propertyName,
            to: col.propertyName,
          }));
        } else {
          info.joinProperties = r.inverseRelation!.joinColumns.map((col:ColumnMetadata) => ({
            from: col.propertyName,
            to: col.referencedColumn!.propertyName,
          }));
        }
      }

      return info;
    });
  }
}
