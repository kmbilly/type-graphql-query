import { plainToClass } from 'class-transformer';
import { validate, ValidationError } from 'class-validator';
import { GraphQLResolveInfo } from 'graphql'
import { Authorized, Resolver, Query, Mutation, Info, Arg, ClassType, Int, ArgumentValidationError } from 'type-graphql';
import { CreateInputArg, FilterArg, IdArg, UpdateInputArg } from '../decorators';
import { getDataByGqlQuery, getStatByGqlQuery } from '../typeorm/query/getDataByGqlQuery';
import { ListStat, PagingArgs, SortingArgs, ModelAccessType } from '../types';
import { camelCase } from './stringUtil'
import { getConnection } from './databaseConnection';
import { Repository } from 'typeorm';
import { getMetadataStorage } from '../metadata';

export declare type ReturnClassTypeFunc<TItem> = () => ClassType<TItem>

export interface GenericResolverOptions<TItem> {
  // access: {
  //   get: string,
  //   create: string,
  //   update: string,
  //   delete: string
  // }
  plural?: string,
  validate?: (value:TItem, options:{ create?:boolean, update?:boolean }) => Promise<ValidationError[]>,
  beforeCreate?: (value:TItem, repo: Repository<TItem>) => Promise<TItem>,
  beforeUpdate?: (id:any, value:TItem, repo: Repository<TItem>) => Promise<TItem>,
  beforeDelete?: (id:any, repo: Repository<TItem>) => Promise<TItem>,
}

export function GenericResolver<TItem>(TItemClassFunc: ReturnClassTypeFunc<TItem>, options:GenericResolverOptions<TItem>):any {
  const TItemClass = TItemClassFunc()
  const pluralName = options.plural ? camelCase(options.plural) : camelCase(TItemClass.name) + 's'

  const metadataStorage = getMetadataStorage()
  const modelAccess = (metadataStorage.accesses.find((item:ModelAccessType) => item.target === TItemClass))?.access

  // const connectionName = `${TItemClass.name}Connection`
  // const connectionContainer = {
  //   [connectionName]: class extends Connection(TItemClass){},
  // };
  // ObjectType()(connectionContainer[connectionName])

  @Resolver({ isAbstract: true })
  abstract class GenericResolverClass {
    async _getOne(
      info: GraphQLResolveInfo,
      id: any,
    ) {
      const filter:any = {}
      if (typeof id !== 'object') {
        filter.id = { eq: id }
      } else {
        Object.keys(id).forEach(subId => {
          filter[subId] = { eq: id[subId] }
        })
      }
      return getDataByGqlQuery(info, { filter }, TItemClass)
    }

    async _getMany(
      info: GraphQLResolveInfo,
      filter: any,
      paging: PagingArgs = new PagingArgs(),
      sorting: SortingArgs<TItem>[],
    ) {
      return getDataByGqlQuery(info, { filter, paging, sorting }, [TItemClass])
    }

    async _getStat(
      info: GraphQLResolveInfo,
      filter: any,
    ) {
      return getStatByGqlQuery(info, { filter }, TItemClass)
    }

    async _checkUnique(
      repo:Repository<TItem>,
      input:any,
      primaryCols: any[]
    ) {
      const primaryColNames:any = primaryCols.map(p => p.propertyName)
      const uniqueFields = getMetadataStorage().uniqueFields.filter(f => f.target === TItemClass).map(f => [f.field])
      for (let uf of uniqueFields.concat([primaryColNames])) {
        if (uf.some(ufi => Object.keys(input).includes(ufi))) {
          const where:any = {}
          uf.forEach(ufi => { where[ufi] = input[ufi] })
          const existsRows = await repo.find({ select: primaryColNames, where, lock: { mode: "pessimistic_read" } })
          if (existsRows.length > 0) {
            throw new ArgumentValidationError(uf.map(ufi => {
              const ve = new ValidationError()
              ve.property = ufi
              ve.value = input[ufi]
              ve.constraints =  { unique: '' }
              return ve
            }));
          }
        }
      }
    }

    @Query(returns => TItemClass, { name: camelCase(TItemClass.name) })
    @Authorized(modelAccess && modelAccess.get)
    async [camelCase(TItemClass.name)](
      @Info()
      info: GraphQLResolveInfo,
      @IdArg(() => TItemClass )
      id: any,
    ) {
      return this._getOne(info, id)
    }

    @Query(() => [TItemClass], { name: pluralName })
    @Authorized(modelAccess && modelAccess.get)
    async [pluralName](
      @Info()
      info: GraphQLResolveInfo,
      @FilterArg(TItemClassFunc)
      filter: any,
      @Arg('paging', { nullable: true })
      paging: PagingArgs = new PagingArgs(),
      @Arg('sorting', () => [SortingArgs], { nullable: true })
      sorting: SortingArgs<TItem>[],
    ) {
      return this._getMany(info, filter, paging, sorting)
    }  

    @Query(() => ListStat, { name: `${pluralName}Stat` })
    @Authorized(modelAccess && modelAccess.get)
    async [`${pluralName}Stat`](
      @Info()
      info: GraphQLResolveInfo,
      @FilterArg(TItemClassFunc)
      filter: any,
    ) {
      return this._getStat(info, filter)
    }

    async validateOnCreate(item:any) {
      let errors:ValidationError[] = 
        await validate(item, {
          groups: ['create'],
          skipMissingProperties: true,
          always: true,
          validationError: {
            target: false,
            value: false,
          }
        })
      
      if (options.validate) {
        errors = errors.concat(
          await options.validate(item, {
            create: true
          })
        )
      }

      return errors
    }

    @Mutation(returns => TItemClass, { name: `create${TItemClass.name}` })
    @Authorized(modelAccess && modelAccess.create)
    async [`create${TItemClass.name}`](
      @Info()
      info: GraphQLResolveInfo,
      @CreateInputArg(TItemClass)
      input: any,
    ) {
      const item:any = plainToClass(TItemClass, input);
      const errors = await this.validateOnCreate(item)
      if (errors.length > 0) {
        throw new ArgumentValidationError(errors);
      }

      // const repo = getConnection().getRepository(TItemClass)
      let newItemId:any
      await getConnection().transaction(async transactionEntityManager => {
        const repo = transactionEntityManager.getRepository(TItemClass)

        if (options.beforeCreate) {
          input = await options.beforeCreate(input, repo)
        }

        // check unique fields including primary cols
        const primaryCols = repo.metadata.primaryColumns
        await this._checkUnique(repo, input, primaryCols)

        const newItem:any = await repo.insert(input)

        if (primaryCols.length === 1) {
          newItemId = newItem.generatedMaps[0][primaryCols[0].propertyName]
        } else {
          newItemId = {}
          primaryCols.forEach((col) => {
            newItemId[col.propertyName] = newItem.generatedMaps[0][col.propertyName]
          })
        }
      })

      return this._getOne(info, newItemId)
    }

    @Mutation(returns => TItemClass, { name: `update${TItemClass.name}` })
    @Authorized(modelAccess && modelAccess.update)
    async [`update${TItemClass.name}`](
      @Info()
      info: GraphQLResolveInfo,
      @IdArg(() => TItemClass)
      id: any,
      @UpdateInputArg(TItemClass)
      input: any,
    ) {
      const updatedObj = await (getConnection().getRepository(TItemClass)).findOne(id);
      Object.assign(updatedObj, input);

      const item:any = plainToClass(TItemClass, updatedObj);

      let errors = await validate(item, {
        groups: ['update'],
        skipMissingProperties: true,
        always: true,
        validationError: {
          target: false,
          value: false,
        }
      })
      if (options.validate) {
        errors = errors.concat(
          await options.validate(item, {
            update: true
          })
        )
      }
      if (errors.length > 0) {
        throw new ArgumentValidationError(errors);
      }

      // const repo = getConnection().getRepository(TItemClass)
      await getConnection().transaction(async transactionEntityManager => {
        const repo = transactionEntityManager.getRepository(TItemClass)

        if (options.beforeUpdate) {
          input = await options.beforeUpdate(id, input, repo)
        }
  
        const primaryCols = repo.metadata.primaryColumns
        await this._checkUnique(repo, input, primaryCols)

        await repo.update(id, input)
      })

      return this._getOne(info, id)
    }

    @Mutation(returns => Int, { name: `delete${TItemClass.name}` })
    @Authorized(modelAccess && modelAccess.delete)
    async [`delete${TItemClass.name}`](
      @Info()
      info: GraphQLResolveInfo,
      @IdArg(() => TItemClass )
      id: any,
    ) {
      const repo = getConnection().getRepository(TItemClass)

      if (options.beforeDelete) {
        await options.beforeDelete(id, repo)
      }

      const { affected } = await repo.delete(id)
      return affected
    }
  }

  return GenericResolverClass
}
