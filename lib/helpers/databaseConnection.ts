import { Connection } from "typeorm"

let connection:Connection

export function setConnection(newConn:Connection) {
  connection = newConn
}

export function getConnection() {
  return connection
}