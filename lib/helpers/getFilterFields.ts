import { Filter } from '../interfaces'

export const getFilterFields = <DTO>(filter: Filter<DTO>): string[] => {
  const fieldSet: Set<string> = Object.keys(filter).reduce((fields: Set<string>, filterField: string): Set<string> => {
    if (filterField === 'and' || filterField === 'or') {
      const andOrFilters = filter[filterField];
      if (andOrFilters !== undefined) {
        return andOrFilters.reduce((andOrFields, andOrFilter) => {
          return new Set<string>([...andOrFields, ...getFilterFields(andOrFilter)]);
        }, fields);
      }
    } else if (filterField === 'not') {
      const notFilter = filter.not;
      if (notFilter !== undefined) {
        getFilterFields(notFilter).forEach(field => fields.add(field))
      }
    } else {
      fields.add(filterField);
    }
    return fields;
  }, new Set<string>());
  return [...fieldSet];
};