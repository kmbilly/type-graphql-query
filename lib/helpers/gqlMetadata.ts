import { getMetadataStorage as getTypeGraphQLMetadataStorage } from "type-graphql/dist/metadata/getMetadataStorage";

export const getFieldType = (type:any, field:string) => {
  const typeGraphqlField = getField(type, field)
  const fieldType = typeGraphqlField ? typeGraphqlField.getType() : undefined

  return fieldType
}

export const getField = (type:any, field:any) => {
  const typeGraphQLMetadata = getTypeGraphQLMetadataStorage();
  return typeGraphQLMetadata.fields.find(md => { return md.target === type && md.name === field })
}
