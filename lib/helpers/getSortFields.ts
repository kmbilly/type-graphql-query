import { SortField } from '../interfaces'

export const getSortFields = <Entity>(sorts: SortField<Entity>[]): string[] => {
  return sorts.map((sort:SortField<Entity>) => (sort.field as string).split('.')[0])
}