import { Field, InputType } from "type-graphql";
import { getConnection } from "./databaseConnection";
import { getFieldType } from "./gqlMetadata";
const generatedIdTypes:any = {}

/**
 * Generate a type-graphql InputType from a @ObjectType decorated
 * class by calling the @InputType and @FiterableField decorators
 *
 * This should be used to generate the type of the @Arg
 * decorator on the corresponding resolver.
 *
 * @param type
 */
export function generateIdType(returnTypeFunc: Function) {
  return () => {
    // const type = typeFunction()
    const type = returnTypeFunc()
    const repo = getConnection().getRepository(type)
    if (!repo) {
      throw new Error(
        'Target class must be decorated with typeorm Entity'
      )
    }
    const typeName = repo.metadata.name

    if (generatedIdTypes[typeName]) {
      return generatedIdTypes[typeName];
    }

    // const repo = getRepository(type)
    // if (!repo) {
    //   throw new Error(
    //     'Target class must be decorated with typeorm Entity'
    //   )
    // }
    const primaryCols = repo.metadata.primaryColumns


    if (primaryCols.length === 1) {
      return getFieldType(type, primaryCols[0].propertyName)
    } else {
      // Create a new empty class with the "<typeName>Id" name
      const idTypeName = typeName + "Id";
      const idTypeContainer = {
        [idTypeName]: class{},
      };

      generatedIdTypes[typeName] = idTypeContainer[idTypeName]

      // Call the @InputType decorator on that class
      InputType()(idTypeContainer[idTypeName]);
  
      // Simulate creation of fields for this class/InputType by calling @Field()
      primaryCols.forEach((col:any) => {
        const returnTypeFunction:any = () => getFieldType(type, col.propertyName)
        Field(returnTypeFunction, { nullable: false })(
          idTypeContainer[idTypeName].prototype,
          col.propertyName,
        );
      })

      return idTypeContainer[idTypeName];
    }
  }
};
