import { Field, InputType } from "type-graphql";
import { getMetadataStorage as getTypeGraphQLMetadataStorage } from "type-graphql/dist/metadata/getMetadataStorage";
import { getMetadataStorage } from "../metadata/getMetadataStorage";

export type CreateOrUpdate = 'create' | 'update'

const generatedClasses:any = {}

/**
 * Generate a type-graphql InputType from a @ObjectType decorated
 * class by calling the @InputType and @Field decorators
 *
 * This should be used to generate the type of the @Arg
 * decorator on the corresponding resolver.
 *
 * @param type
 */
export const generateMutateType = (type: Function, createOrUpdate: CreateOrUpdate) => {
  const typeGraphQLMetadata = getTypeGraphQLMetadataStorage();
  const objectTypesList = typeGraphQLMetadata.objectTypes;
  const graphQLModel = objectTypesList.find((ot) => ot.target === type);
  if (!graphQLModel) {
    throw new Error(
      `Please decorate your class "${type}" with @ObjectType if you want to create mutate input type on it`,
    );
  }
  const inputTypeName = 
    (createOrUpdate === 'create' ? 'Create' : 'Update') +
    graphQLModel.name + 'Input'

  if (!generatedClasses[inputTypeName]) {
    const metadataStorage = getMetadataStorage();
    const mutateOptionData = metadataStorage.mutateOption
      .filter((f) => f.target === type)
      // .filter((f) => 
      //     (Object.keys(f.options).indexOf(createOrUpdate) < 0 || f.options[createOrUpdate] === true)
      // )

    const graphQLFields = typeGraphQLMetadata.fields.filter((f) => f.target === type) || [];

    // Create a new empty class with the "<graphQLModel.name>Filter" name
    
    const inputTypeContainer = {
      [inputTypeName]: class{},
    };
    generatedClasses[inputTypeName] = inputTypeContainer[inputTypeName];

    // Call the @InputType decorator on that class
    InputType()(inputTypeContainer[inputTypeName]);

    // Simulate creation of fields for this class/InputType by calling @Field()
    // const relations = metadataStorage.relations.filter(r => r.target === type);
    // const relationNames = relations.map(r => r.field);
    for (const field of graphQLFields) {
      // console.log(graphQLModel.name + ':' + field.name);
      // console.log(field.getType);
      // console.log(Array.isArray(field.getType()) ? objectTypesList.find(o => o.target == field.getType()[0])) : objectTypesList.find(o => o.target == field.getType));
      // if (!relationNames.includes(field.name)) {
      const mutateOption = mutateOptionData.find(f => f.field === field.name)
      const readOnly = mutateOption?.options?.readOnly
      const allowed = !mutateOption || mutateOption.options[createOrUpdate] !== false
      if (!readOnly && allowed) {
        if (createOrUpdate === 'create') {
          Field(field.getType, field.typeOptions)(
            inputTypeContainer[inputTypeName].prototype,
            field.schemaName || field.name,
          );
        } else {
          Field(field.getType, { ...field.typeOptions, nullable: true })(
            inputTypeContainer[inputTypeName].prototype,
            field.schemaName || field.name,
          );
        }
      }
      // }
    }

    // for (const relation of relations) {
    //   const mutateOption = mutateOptionData.find(f => f.field === relation.field)
    //   if (!mutateOption || !mutateOption.options.readOnly) {
    //     if (createOrUpdate === 'create') {
    //       let createInputTypeFunc;
    //       if (relation.toOne) {
    //         createInputTypeFunc = generateMutateType(relation.type, 'create')
    //       } else {
    //         createInputTypeFunc = () => [generateMutateType(relation.type, 'create')()]
    //       }
    //       Field(createInputTypeFunc)(
    //         inputTypeContainer[inputTypeName].prototype,
    //         relation.field,
    //       );
    //     } else {
    //       let updateInputTypeFunc;
    //       if (relation.toOne) {
    //         updateInputTypeFunc = generateMutateType(relation.type, 'update')
    //       } else {
    //         updateInputTypeFunc = () => [generateMutateType(relation.type, 'update')()]
    //       }
    //       Field(updateInputTypeFunc)(
    //         inputTypeContainer[inputTypeName].prototype,
    //         relation.field,
    //       );
    //     }
    //   }
    // }
  }

  return () => generatedClasses[inputTypeName];
};
