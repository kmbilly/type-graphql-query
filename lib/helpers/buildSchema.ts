import { buildSchema as _buildSchema, BuildSchemaOptions as _BuildSchemaOptions } from 'type-graphql'
import { Connection } from 'typeorm';
// import { printSchema } from 'graphql'
import { setConnection } from './databaseConnection';

let schemaBuilders:any = [];
const schemaBuilderResult:any = [];
let builtSchema:any

export interface BuildSchemaOptions extends _BuildSchemaOptions {
  connection: Connection;
}

export function addSchemaBuilder(builder:any) {
  const builderIndex = schemaBuilders.length
  schemaBuilders.push(builder);
  return () => schemaBuilderResult[builderIndex]
}

export async function buildSchema(options: BuildSchemaOptions) {
  if (!builtSchema) {
    setConnection(options.connection)
    // use for instead of forEach to cater new builders add within other builders
    for (let i = 0; i < schemaBuilders.length; i++) {
      const builder = schemaBuilders[i]
      const buildResult = builder()
      schemaBuilderResult.push(buildResult)
    }
    schemaBuilders = []

    builtSchema = await _buildSchema(options)

    // console.log('Correct: ' + printSchema(builtSchema).indexOf('people(sorting: [SortingArgs!], paging: PagingArgs, filter: PersonFilter): [Person!]!'))

    // const nextResult = await _buildSchema(options)

    // console.log('Correct: ' + printSchema(nextResult).indexOf('people(sorting: [SortingArgs!], paging: PagingArgs, filter: PersonFilter): [Person!]!'))
  }

  return builtSchema
}
