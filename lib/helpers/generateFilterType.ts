import { Field, InputType, Int, Float, ID } from "type-graphql";
import { getMetadataStorage as getTypeGraphQLMetadataStorage } from "type-graphql/dist/metadata/getMetadataStorage";
import { getMetadataStorage } from "../metadata/getMetadataStorage";
import { 
  DateOnly,
  StringFieldComparison,
  BooleanFieldComparison,
  DateFieldComparison,
  NumberFieldComparison,
  IntFieldComparison,
  EnumFieldComparison,
  ArrayComparison,
} from "../types";
import { GraphQLScalarType } from "graphql";
import { getField } from "./gqlMetadata";
import { Decimal } from "../types/scalars/Decimal";
// import { TypeValue } from "type-graphql/dist/decorators/types";

const generatedFilterTypes:any = {}
const generatedArrayComparisonTypes:any = {}

/**
 * Generate a type-graphql InputType from a @ObjectType decorated
 * class by calling the @InputType and @FiterableField decorators
 *
 * This should be used to generate the type of the @Arg
 * decorator on the corresponding resolver.
 *
 * @param type
 */
export const generateFilterType = (typeFunction: Function) => {
  return () => {
    const type = typeFunction()

    if (generatedFilterTypes[type.name]) {
      return generatedFilterTypes[type.name];
    }

    const metadataStorage = getMetadataStorage();
    const filtersData = metadataStorage.filters.filter((f) => f.target === type);

    const typeGraphQLMetadata = getTypeGraphQLMetadataStorage();

    const objectTypesList = typeGraphQLMetadata.objectTypes;
    const graphQLModel = objectTypesList.find((ot) => ot.target === type);

    if (!graphQLModel) {
      throw new Error(
        `Please decorate your class "${type}" with @ObjectType if you want to filter it`,
      );
    }

    // Create a new empty class with the "<graphQLModel.name>Filter" name
    const conditionTypeName = graphQLModel.name + "Filter";
    const conditionTypeContainer = {
      [conditionTypeName]: class{},
    };

    generatedFilterTypes[type.name] = conditionTypeContainer[conditionTypeName]

    // Call the @InputType decorator on that class
    InputType()(conditionTypeContainer[conditionTypeName]);

    // Simulate creation of fields for this class/InputType by calling @Field()
    for (const { field } of filtersData) {
      // When dealing with methods decorated with @Field, we need to lookup the GraphQL
      // name and use that for our filter name instead of the plain method name
      const graphQLField = typeGraphQLMetadata.fieldResolvers.find(
        (fr) => fr.target === type && fr.methodName === field,
      );

      const fieldName = graphQLField ? graphQLField.schemaName : field;
      const typeGraphqlField = getField(type, field)
      let fieldType = typeGraphqlField?.getType()
      let fieldTypeIsArray = typeGraphqlField?.typeOptions?.array

      // handle connection type
      // if ((fieldType as any).baseType) {
      //   fieldType = (fieldType as any).baseType() as TypeValue;
      //   fieldTypeIsArray = true;
      // }
      if (Array.isArray(fieldType)) {
        fieldType = fieldType[0];
        fieldTypeIsArray = true;
      }

      let returnTypeFunction:any
      let fieldTypeName:string = ''
      if (typeof fieldType === 'function') {
        fieldTypeName = (fieldType as Function).name;
        if (fieldType === String) {
          returnTypeFunction = () => StringFieldComparison;
        } else if (fieldType === Number) {
          returnTypeFunction = () => NumberFieldComparison;
        } else if (fieldType === Boolean) {
          returnTypeFunction = () => BooleanFieldComparison;
        } else if (fieldType === Date) {
          returnTypeFunction = () => DateFieldComparison;
        } else {
          const generatedFilterType = generatedFilterTypes[(fieldType as Function).name]
          if (generatedFilterType) {
            returnTypeFunction = () => generatedFilterType;
          } else {
            const newType = generateFilterType(() => fieldType as Function)()
            returnTypeFunction = () => newType
          }
        }
      } else if (typeof fieldType === 'object') {
        fieldTypeName = (fieldType as GraphQLScalarType).name;
        if ((fieldType as GraphQLScalarType).name === Int.name) {
          returnTypeFunction = () => IntFieldComparison;
        } else if ((fieldType as GraphQLScalarType).name === DateOnly.name) {
          returnTypeFunction = () => DateFieldComparison;
        } else if ((fieldType as GraphQLScalarType).name === Float.name) {
          returnTypeFunction = () => NumberFieldComparison;
        } else if ((fieldType as GraphQLScalarType).name === ID.name) {
          returnTypeFunction = () => StringFieldComparison;
        } else if ((fieldType as GraphQLScalarType).name === Decimal.name) {
          returnTypeFunction = () => NumberFieldComparison;
        } else {
          returnTypeFunction = () => EnumFieldComparison;
        }
      }
      if (!returnTypeFunction) {
        fieldTypeName = 'String'
        returnTypeFunction = () => StringFieldComparison;
      }

      if (fieldTypeIsArray) {
        const arrayComparisonType = generatedArrayComparisonTypes[fieldTypeName]
        if (arrayComparisonType) {
          returnTypeFunction = () => arrayComparisonType
        } else {
          returnTypeFunction = generateArrayComparisonType(fieldTypeName, returnTypeFunction())
        }
      }

      Field(returnTypeFunction, { nullable: true })(
        conditionTypeContainer[conditionTypeName].prototype,
        fieldName,
      );
    }

    // recursive and field
    Field(() => [conditionTypeContainer[conditionTypeName]], { nullable: true })(
      conditionTypeContainer[conditionTypeName].prototype,
      'and',
    );

    // recursive or field
    Field(() => [conditionTypeContainer[conditionTypeName]], { nullable: true })(
      conditionTypeContainer[conditionTypeName].prototype,
      'or',
    );

    // recursive not field
    Field(() => conditionTypeContainer[conditionTypeName], { nullable: true })(
      conditionTypeContainer[conditionTypeName].prototype,
      'not',
    );
    
    return conditionTypeContainer[conditionTypeName];
  }
};

const generateArrayComparisonType = (typeName:string, type:any) => {
  // Create a new empty class with the "<typeName>ArrayComparison" name
  const newTypeName = typeName + "ArrayComparison";
  const newTypeContainer = {
    [newTypeName]: class extends ArrayComparison(type){},
  };

  InputType()(newTypeContainer[newTypeName]);
  generatedArrayComparisonTypes[typeName] = newTypeContainer[newTypeName]

  // Field(type, { nullable: true })(
  //   newTypeContainer[newTypeName].prototype,
  //   'every',
  // );

  // Field(type, { nullable: true })(
  //   newTypeContainer[newTypeName].prototype,
  //   'some',
  // );

  // Field(type, { nullable: true })(
  //   newTypeContainer[newTypeName].prototype,
  //   'none',
  // );

  // Field(() => Boolean, { nullable: true })(
  //   newTypeContainer[newTypeName].prototype,
  //   'isNull',
  // );

  return () => newTypeContainer[newTypeName]
}

/*

  // Create a new empty class with the "<graphQLModel.name>Condition" name
  const conditionTypeName = graphQLModel.name + "Condition";
  const conditionTypeContainer = {
    [conditionTypeName]: class {},
  };

  // Call the @InputType decorator on that class
  InputType()(conditionTypeContainer[conditionTypeName]);

  // Simulate creation of fields for this class/InputType by calling @Field()
  for (const { field, getReturnType } of filtersData) {
    // When dealing with methods decorated with @Field, we need to lookup the GraphQL
    // name and use that for our filter name instead of the plain method name
    const graphQLField = typeGraphQLMetadata.fieldResolvers.find(
      (fr) => fr.target === type && fr.methodName === field,
    );

    const fieldName = graphQLField ? graphQLField.schemaName : field;

    Field(() => BaseOperator, { nullable: true })(
      conditionTypeContainer[conditionTypeName].prototype,
      "operator",
    );

    for (const operator of operators) {
      const baseReturnType =
        typeof getReturnType === "function" ? getReturnType() : String;
      const returnTypeFunction = ARRAY_RETURN_TYPE_OPERATORS.includes(operator)
        ? () => [baseReturnType]
        : () => baseReturnType;

      Field(returnTypeFunction, { nullable: true })(
        conditionTypeContainer[conditionTypeName].prototype,
        `${String(fieldName)}_${operator}`,
      );
    }
  }

  // Extend the Condition type to create the final Filter type
  const filterTypeName = graphQLModel.name + "Filter";
  const filterTypeContainer = {
    [filterTypeName]: class extends conditionTypeContainer[conditionTypeName] {},
  };

  InputType()(filterTypeContainer[filterTypeName]);

  Field(() => [conditionTypeContainer[conditionTypeName]], {
    nullable: true,
  })(filterTypeContainer[filterTypeName].prototype, "conditions");

  return () => filterTypeContainer[filterTypeName];

};
*/