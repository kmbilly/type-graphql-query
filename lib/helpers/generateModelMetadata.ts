import { GraphQLScalarType } from "graphql";
import { Float, ID, Int } from "type-graphql";
import { getMetadataStorage as getTypeGraphQLMetadataStorage } from "type-graphql/dist/metadata/getMetadataStorage";
import { getMetadataStorage as getValidatorMetadataStorage } from 'class-validator'
import { getMetadataStorage } from "../metadata/getMetadataStorage";
import { DateOnly } from "../types";
import { Decimal } from "../types/scalars/Decimal";
import { camelCase } from './stringUtil'

const fs = require('fs');

export const generateModelMetadata = (type:any, modelFile:string) => {
  const validatorMs = getValidatorMetadataStorage()
  const typeValidators = validatorMs.getTargetValidationMetadatas(type, '', true, false, ['create'])
  // typeValidators.map(m => {
  //   console.log(m.propertyName)
  //   console.log(validatorMs.getTargetValidatorConstraints(m.constraintCls).map(c => c.name))
  //   console.log(m.constraints)
  // });

  let feModels:any = {}
  try {
    feModels = JSON.parse(fs.readFileSync(modelFile));
  } catch (err) {
    if (!(err instanceof Error) || (err as any).code !== 'ENOENT') {
      throw err
    }
  }

  const typeGraphqlMs = getTypeGraphQLMetadataStorage()
  const ms = getMetadataStorage()
  // types.forEach((type:any) => {
  let modelFields = feModels.fields
  if (!modelFields) {
    modelFields = []
    feModels.fields = modelFields
  }
  const modelFilters = ms.filters.filter(filter => filter.target === type)
  const mutateOptionData = ms.mutateOption
    .filter((f) => f.target === type)

  typeGraphqlMs.fields.filter(field => field.target === type).forEach(field => {
    const fieldMeta:any = {}

    // required
    if (field.typeOptions.nullable === false) {
      fieldMeta.required = true
    }

    // is array
    if (field.typeOptions.array) {
      fieldMeta.array = true
    }

    // filterable
    if (!modelFilters || !modelFilters.find(filter => filter.field === field.name)) {
      fieldMeta.filterable = false
    }

    // find out the type and options
    const fieldType = field.getType()
    let fieldTypeName
    if (typeof fieldType === 'function') {
      fieldTypeName = (fieldType as Function).name;
      if (! [String, Number, Boolean, Date].includes(fieldType as any)) {
        fieldTypeName = 'relation'
      } else if (Date === fieldType) {
        fieldTypeName = 'datetime'
      }
    } else if (typeof fieldType === 'object') {
      fieldTypeName = (fieldType as GraphQLScalarType).name;
      if (![Int.name, DateOnly.name, Decimal.name, Float.name, ID.name].includes((fieldType as GraphQLScalarType).name)) {
        fieldTypeName = 'option'
      } else if (DateOnly.name === (fieldType as GraphQLScalarType).name) {
        fieldTypeName = 'date'
      } else if (Decimal.name === (fieldType as GraphQLScalarType).name) {
        fieldTypeName = 'decimal'
      }
    }
    fieldMeta.type = fieldTypeName && camelCase(fieldTypeName)

    // find out if field allows in create/update
    const mutateOption = mutateOptionData.find(f => f.field === field.name)
    if (!mutateOption || (!mutateOption.options.readOnly && (mutateOption.options.create === undefined || mutateOption.options.create === true))) {
      fieldMeta.create = true
    } else {
      fieldMeta.create = undefined
    }
    if (!mutateOption || (!mutateOption.options.readOnly && (mutateOption.options.update === undefined || mutateOption.options.update === true))) {
      fieldMeta.update = true
    } else {
      fieldMeta.update = undefined
    }

    // add model field if not exists in existing metadata
    let model = modelFields.find((feField:any) => feField.name === field.name);
    if (!model) {
      model = {
        name: field.name,
        label: field.name,
      }
      modelFields.push(model)
    }

    // if current model defined options, override type as option
    if (Array.isArray(model.options)) {
      fieldMeta.type = 'option'
    }
    
    // update validate constraints
    if (fieldMeta.create || fieldMeta.update) {
      if (!fieldMeta.validate) {
        fieldMeta.validate = {}
      }
      const fieldValidators = typeValidators.filter(m => m.propertyName === field.name)
      fieldValidators.map(v => {
        let validatorName = validatorMs.getTargetValidatorConstraints(v.constraintCls).map(c => c.name)[0]
        if (validatorName === 'isDefined') {
          validatorName = 'required'
        }
        if (validatorName) {
          fieldMeta.validate[validatorName] = !v.constraints || v.constraints[0] === undefined ? true : v.constraints[0]
        }
      })
      if (fieldMeta.type === 'string' && !fieldMeta.validate.maxLength) {
        fieldMeta.validate.maxLength = 255
      }
    }

    // update options
    if (fieldTypeName === 'option') {
      let fieldOptions = model.options || []
      Object.keys(fieldType).forEach((optionKey:string) => {
        if (!fieldOptions.find((opt:any) => opt.name === optionKey)) {
          fieldOptions.push({
            name: optionKey,
            label: optionKey,
          })
        }
      })
      fieldMeta.options = fieldOptions
    }

    // update metadata
    Object.assign(model, fieldMeta)
  })
  // });

  fs.writeFileSync(modelFile, JSON.stringify(feModels, null, 2))
}