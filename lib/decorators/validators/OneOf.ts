import {
  registerDecorator,
  ValidationOptions,
  ValidationArguments,
} from 'class-validator';

export const ONE_OF = 'oneOf';

export function OneOf(allowList: string[], validationOptions?: ValidationOptions) {
  return function(object: Object, propertyName: string) {
    registerDecorator({
      name: ONE_OF,
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [allowList],
      validator: {
        validate(value: any, args: ValidationArguments) {
          const [allowList] = args.constraints;
          return allowList.includes(value);
        },
      },
    });
  };
}