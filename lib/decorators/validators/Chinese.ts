import {
  registerDecorator,
  ValidationOptions,
  ValidationArguments,
  isHalfWidth,
} from 'class-validator';

export const CHINESE = 'chinese';

export function Chinese(validationOptions?: ValidationOptions) {
  return function(object: Object, propertyName: string) {
    registerDecorator({
      name: CHINESE,
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [],
      validator: {
        validate(value: any, args: ValidationArguments) {
          return !isHalfWidth(value)
        },
      },
    });
  };
}