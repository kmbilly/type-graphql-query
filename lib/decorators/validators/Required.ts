import { IsDefined } from 'class-validator';

export function Required(
): PropertyDecorator {
  return (prototype:any, field: string | symbol) => {
    IsDefined()(
      prototype,
      field,
    );
  }
}
