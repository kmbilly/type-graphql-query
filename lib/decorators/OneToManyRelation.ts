import { Arg, Authorized, Field } from "type-graphql";
import { getMetadataStorage } from "../metadata";
// import { ReturnTypeFunc } from "type-graphql/dist/decorators/types";
import { ListStat, PagingArgs, SortingArgs, ModelAccessType } from "../types";
import { FilterArg } from "./FilterArg";
import { RelationOptions } from "../interfaces/RelationOptions.interface";
import { ObjectType, OneToMany } from "typeorm";
import { addSchemaBuilder } from "../helpers";

export function OneToManyRelation<T>(
  returnTypeFunction: ((type?: any) => ObjectType<T>),
  inverseSide: string | ((object: T) => any),
  options?: RelationOptions,
): PropertyDecorator {

  return (prototype:any, field: string | symbol) => {

    // add typeorm relation definition
    const typeormProperty = `_${field as string}`
    prototype[typeormProperty] = undefined
    OneToMany(returnTypeFunction, inverseSide, options)(
      prototype, typeormProperty
    )

    addSchemaBuilder(() => {
      const returnType = returnTypeFunction()
      // const returnConnectionFunc = returnConnectionClass(returnType.name, returnTypeFunction);  
  
      // override the field as a function to support query of relation fields
      Reflect.defineMetadata("design:paramtypes", [String,String,String], prototype, field)
      prototype[field] = (
        filter:any,
        paging:any = new PagingArgs(),
        sorting:any,
      ) => {}
  
      FilterArg(returnTypeFunction)(
        prototype, field, 0
      )
      Arg('paging', () => PagingArgs, { nullable: true })(
        prototype, field, 1
      )
      Arg('sorting', () => [SortingArgs], { nullable: true })(
        prototype, field, 2
      )
  
      Field(() => [returnType], { nullable: true })(
        prototype,
        field,
      );

      // add stat field
      const statProperty = `${field as string}Stat`;
      Reflect.defineMetadata("design:paramtypes", [String], prototype, statProperty)
      prototype[statProperty] = (
        filter:any,
      ) => {}
      FilterArg(returnTypeFunction)(
        prototype, statProperty, 0
      )
      Field(() => ListStat, { nullable: true })(
        prototype,
        statProperty,
      );
   
      const metadataStorage = getMetadataStorage();
      if (options?.filterable !== false) {
        metadataStorage.filters.push({
          field,
          // getReturnType: returnTypeFunction,
          target: prototype.constructor,
        });
      }

      const accessOption = (metadataStorage.accesses.find((item:ModelAccessType) => item.target === returnType))
      if (accessOption) {
        if (accessOption.access.get) {
          Authorized(accessOption.access.get)(
            prototype,
            field,
          );
        }
      }

      // if (options && ('create' in options || 'update' in options)) {
      //   metadataStorage.mutateOption.push({
      //     field,
      //     target: prototype.constructor,
      //     options: {
      //       readOnly: options.readOnly,
      //       create: options.create,
      //       update: options.update,
      //     }
      //   });
      // }
      // metadataStorage.relations.push({
      //   field,
      //   target: prototype.constructor,
      //   type: returnType,
      //   toMany: true,
      // })
      metadataStorage.mutateOption.push({
        field: statProperty,
        target: prototype.constructor,
        options: {
          readOnly: true,
        }
      });
    })

  };
}
