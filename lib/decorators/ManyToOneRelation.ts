import { Authorized, Field } from "type-graphql";
import { getMetadataStorage } from "../metadata";
// import { ReturnTypeFunc } from "type-graphql/dist/decorators/types";
// import { PagingArgs, returnConnectionClass, SortingArgs } from "../types";
// import { FilterArg } from "./FilterArg";
import { RelationOptions } from "../interfaces/RelationOptions.interface";
import { ManyToOne, ObjectType } from "typeorm";
import { addSchemaBuilder, ModelAccessType } from "..";

export function ManyToOneRelation<T>(
  returnTypeFunction: ((type?: any) => ObjectType<T>),
  inverseSide: string | ((object: T) => any),
  options?: RelationOptions,
): PropertyDecorator {
  return (prototype:any, field: string | symbol) => {

    // add typeorm relation definition
    ManyToOne(returnTypeFunction, inverseSide, options)(
      prototype, field
    )

    addSchemaBuilder(() => {
      Field(returnTypeFunction, { nullable: true })(
        prototype,
        field,
      );
  
      const metadataStorage = getMetadataStorage();
      if (options?.filterable !== false) {
        metadataStorage.filters.push({
          field,
          // getReturnType: returnTypeFunction,
          target: prototype.constructor,
        });
      }
      metadataStorage.mutateOption.push({
        field,
        target: prototype.constructor,
        options: {
          readOnly: true,
        }
      });

      const returnType = returnTypeFunction()
      const accessOption = (metadataStorage.accesses.find((item:ModelAccessType) => item.target === returnType))
      if (accessOption) {
        if (accessOption.access.get) {
          Authorized(accessOption.access.get)(
            prototype,
            field,
          );
        }
      }

      // metadataStorage.relations.push({
      //   field,
      //   target: prototype.constructor,
      //   type: returnTypeFunction(),
      //   toMany: true,
      // })
    });
  }
}
