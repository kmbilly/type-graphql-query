import { Column, ColumnType } from 'typeorm';
import { ReturnTypeFunc } from 'type-graphql/dist/decorators/types';
import { DataFieldOptions } from '../interfaces/DataFieldOptions.interface';
import { DateOnly } from '../types';
import { Field, Int, registerEnumType } from 'type-graphql';
import { getMetadataStorage } from '../metadata';
import { getMetadataStorage as getValidatorMetadataStorage, IS_DEFINED, MAX_LENGTH } from 'class-validator';
import { ONE_OF } from './validators/OneOf';
import { Decimal } from '../types/scalars/Decimal';

/**
 * This decorator defines a column in database and a GraphQL field
 *
 * @param type
 */
 export function DataField(): PropertyDecorator;
 export function DataField(options: DataFieldOptions): PropertyDecorator;
 export function DataField(
   returnTypeFunction?: ReturnTypeFunc,
   options?: DataFieldOptions
 ): PropertyDecorator;
 export function DataField(
  returnTypeFuncOrOptions?: ReturnTypeFunc | DataFieldOptions,
  maybeOptions?: DataFieldOptions
): PropertyDecorator {
  return (prototype:any, field: string | symbol) => {
    // let returnTypeFunction;

    let returnTypeFunc:ReturnTypeFunc | undefined;
    let options:DataFieldOptions = {};
    if (typeof returnTypeFuncOrOptions === "function") {
      returnTypeFunc = returnTypeFuncOrOptions as ReturnTypeFunc;
      options = maybeOptions || {};
    } else {
      options = returnTypeFuncOrOptions as DataFieldOptions || maybeOptions || {};
      returnTypeFunc = options?.type;
    }

    // try to get required, maxLength, oneOf from class-validator metadata
    const fieldValidators = getValidatorMetadataStorage().getTargetValidationMetadatas(prototype.constructor, '', true, false, ['create']).filter(m => m.propertyName === field);
    const setAsIsDefined = !!(fieldValidators.find(m => m.type === IS_DEFINED));
    let maxLength = fieldValidators.find(m => getValidatorMetadataStorage().getTargetValidatorConstraints(m.constraintCls).find(c => c.name === MAX_LENGTH))?.constraints[0];
    const oneOfList:string[] = fieldValidators.find(m => getValidatorMetadataStorage().getTargetValidatorConstraints(m.constraintCls).find(c => c.name === ONE_OF))?.constraints[0];

    let dataType:ColumnType | undefined;
    // let dataEnum:any[] | Object | undefined;
    if (oneOfList) {
      const returnTypeValue:any = {}
      oneOfList.forEach((opt:string) => returnTypeValue[opt] = opt)
      // dataEnum = returnTypeValue;
      const name = prototype.constructor.name + (field as string).charAt(0).toUpperCase() + (field as string).slice(1)
      registerEnumType(returnTypeValue, {
        name,
        description: name,
      })

      returnTypeFunc = () => returnTypeValue

      if (!maxLength) {
        maxLength = oneOfList.reduce((maxLen, opt) => (opt.length > maxLen ? opt.length : maxLen), 0)
      }
    } else if (returnTypeFunc) {
      const returnTypeValue = returnTypeFunc()
      if (typeof returnTypeValue === 'object') {
        if (returnTypeValue === DateOnly) {
          dataType = 'date';
        } else if (returnTypeValue === Int) {
          dataType = 'bigint'
        } else if (returnTypeValue === Decimal) {
          dataType = 'decimal'
        }
        //  else {
        //   dataType = 'enum';
        //   // dataEnum = returnTypeValue;
        //   const name = prototype.constructor.name + (field as string).charAt(0).toUpperCase() + (field as string).slice(1)
        //   registerEnumType(returnTypeValue, {
        //     name,
        //     description: name,
        //   })
        // }
      }
    }

    const nullable = setAsIsDefined !== true && options.required !== true;
    const length = maxLength || options.length;

    // define database column
    Column({
      type: dataType,
      // enum: dataEnum,
      nullable,
      default: options.default,
      precision: options.precision,
      scale: options.scale,
      length,
      ...(options.columnOptions || {})
    })(
      prototype,
      field
    )

    const metadataStorage = getMetadataStorage();

    if (options.unique) {
      metadataStorage.uniqueFields.push({
        target: prototype.constructor,
        field: field as string,
      })
    }

    // define graphql field
    if (options.private !== true) {
      if (returnTypeFunc) {
        Field(returnTypeFunc, {
          nullable,
        })(
          prototype,
          field,
        );  
      } else {
        Field({
          nullable,
        })(
          prototype,
          field,
        );  
      }

      if (options.filterable !== false) {
        metadataStorage.filters.push({
          field,
          // getReturnType: returnTypeFunction,
          target: prototype.constructor,
        })
      }
      if ('readOnly' in options || 'create' in options || 'update' in options) {
        metadataStorage.mutateOption.push({
          field,
          target: prototype.constructor,
          options: {
            readOnly: options.readOnly,
            create: options.create,
            update: options.update,
          }
        });
      }
    }
  };
}
