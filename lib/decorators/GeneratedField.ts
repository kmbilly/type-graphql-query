import { CreateDateColumn, UpdateDateColumn } from "typeorm";
import { Field } from "type-graphql";
import { getMetadataStorage } from "../metadata";

export function GeneratedField(
  type: 'createDate' | 'updateDate'
): PropertyDecorator {
  return (prototype:any, field: string | symbol) => {
    // define database column
    switch (type) {
        case 'createDate':
            CreateDateColumn(
            )(
                prototype,
                field
            );
            break;
        case 'updateDate':
            UpdateDateColumn(
            )(
                prototype,
                field
            )
            break;
    }

    // define graphql field
    Field()(
      prototype,
      field,
    );  

    const metadataStorage = getMetadataStorage();

    metadataStorage.filters.push({
      field,
      // getReturnType: returnTypeFunction,
      target: prototype.constructor,
    });
    metadataStorage.mutateOption.push({
      field,
      target: prototype.constructor,
      options: {
        readOnly: true,
      }
    });
  };
}
