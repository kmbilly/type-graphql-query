import { Arg } from "type-graphql";
import { addSchemaBuilder, generateMutateType } from "../helpers";

/**
 * This decorator add a create input arg based on the type specified
 * would contains all fields decorated with Field with overrides from MutateOption
 *
 * @param type
 */
export function CreateInputArg(
  type: Function
): ParameterDecorator {
  return (prototype, propertyKey, parameterIndex) => {
    const getBuildResult = addSchemaBuilder(generateMutateType(type, 'create'))
    Arg('input', getBuildResult)(
      prototype, propertyKey, parameterIndex
    );
  };
}
