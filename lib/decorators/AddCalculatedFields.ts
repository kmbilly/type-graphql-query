import { AddCalculatedFieldsOptions } from "../interfaces/AddCalculatedFieldsOptions.interface";
import { CalculatedField } from "./CalculatedField";
import { Int } from "type-graphql";

export function AddCalculatedFields(
  options: AddCalculatedFieldsOptions,
): PropertyDecorator {

  return (prototype:any, field: string | symbol) => {

    for (const key in options) {
      const fieldName = options[key as keyof AddCalculatedFieldsOptions]!
      prototype[fieldName + '?'] = undefined
      const fieldStr = field as string

      let sql:string|undefined;
      let typeFunc:any;
      switch(key) {
        case 'age':
          sql = `TIMESTAMPDIFF(YEAR,${fieldStr},CURDATE())`;
          typeFunc = () => Int;
          break;
        case 'year':
          sql = `YEAR(${fieldStr})`
          typeFunc = () => Int;
          break;
        case 'month':
          sql = `MONTH(${fieldStr})`
          typeFunc = () => Int;
          break;
        case 'yearMonth':
          sql = `CONCAT(YEAR(${fieldStr}), \'-\', LPAD(CONVERT(MONTH(${fieldStr}),CHAR),2,\'0\'))`
          typeFunc = () => String;
          break;
        case 'monthDay':
          sql = `CONCAT(LPAD(CONVERT(MONTH(${fieldStr}),CHAR),2,\'0\'), \'-\', LPAD(CONVERT(DAY(${fieldStr}),CHAR),2,\'0\'))`
          typeFunc = () => String;
          break;
      }

      CalculatedField(typeFunc, {
        sql
      })(prototype, fieldName)
    }

  };
}
