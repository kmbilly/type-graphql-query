// import { getMetadataStorage } from "../metadata";
// import { ReturnTypeFunc } from "type-graphql/dist/decorators/types";

// /**
//  * This decorator will store filters information for the field in a metadata storage.
//  * We will use this metadata later on to generate an InputType for the filters argument
//  *
//  * @param operators
//  * @param returnTypeFunction
//  */
// export function Filter(
//   returnTypeFunction?: ReturnTypeFunc,
// ): PropertyDecorator {
//   return (prototype:any, field: string | symbol) => {
//     const metadataStorage = getMetadataStorage();

//     metadataStorage.filters.push({
//       field,
//       getReturnType: returnTypeFunction,
//       target: prototype.constructor,
//     });
//   };
// }
