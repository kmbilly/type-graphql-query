import { getMetadataStorage } from "../metadata";

export type MutateOptionOptions = {
  readOnly?: boolean,
  create?: boolean,
  update?: boolean
}

/**
 * This decorator will store input field information for the field in a metadata storage.
 * We will use this metadata later on to generate an InputType for the create/update input argument
 *
 * @param returnTypeFunction
 * @param options
 */
export function MutateOption(options: MutateOptionOptions): PropertyDecorator;
export function MutateOption(
  options: MutateOptionOptions
): PropertyDecorator {
  return (prototype:any, field: string | symbol) => {
    const metadataStorage = getMetadataStorage();

    metadataStorage.mutateOption.push({
      field,
      target: prototype.constructor,
      options
    });
  };
}
