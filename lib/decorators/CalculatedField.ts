import { Field, registerEnumType } from "type-graphql";
import { getMetadataStorage } from "../metadata";
import { CalculatedFieldOptions } from "../interfaces/CalculatedFieldOptions.interface";
import { ReturnTypeFunc } from "type-graphql/dist/decorators/types";
import { getMetadataStorage as getValidatorMetadataStorage } from 'class-validator';
import { ONE_OF } from './validators/OneOf';

export function CalculatedField(): PropertyDecorator;
export function CalculatedField(options: CalculatedFieldOptions): PropertyDecorator;
export function CalculatedField(
   returnTypeFunction?: ReturnTypeFunc,
   options?: CalculatedFieldOptions
): PropertyDecorator;
export function CalculatedField(
  returnTypeFuncOrOptions?: ReturnTypeFunc | CalculatedFieldOptions,
  maybeOptions?: CalculatedFieldOptions
): PropertyDecorator {
  return (prototype:any, field: string | symbol) => {
    let returnTypeFunc:ReturnTypeFunc | undefined;
    let options:CalculatedFieldOptions = {};
    if (typeof returnTypeFuncOrOptions === "function") {
      returnTypeFunc = returnTypeFuncOrOptions as ReturnTypeFunc;
      options = maybeOptions || {};
    } else {
      options = returnTypeFuncOrOptions as CalculatedFieldOptions || maybeOptions || {};
      returnTypeFunc = options.type;
    }

    const fieldValidators = getValidatorMetadataStorage().getTargetValidationMetadatas(prototype.constructor, '', true, false, ['create']).filter(m => m.propertyName === field);
    const oneOfList:string[] = fieldValidators.find(m => getValidatorMetadataStorage().getTargetValidatorConstraints(m.constraintCls).find(c => c.name === ONE_OF))?.constraints[0];
    if (oneOfList) {
      const returnTypeValue:any = {}
      oneOfList.forEach((opt:string) => returnTypeValue[opt] = opt)
      // dataEnum = returnTypeValue;
      const name = prototype.constructor.name + (field as string).charAt(0).toUpperCase() + (field as string).slice(1)
      registerEnumType(returnTypeValue, {
        name,
        description: name,
      })

      returnTypeFunc = () => returnTypeValue
    }

    // define graphql field
    if (returnTypeFunc) {
      Field(returnTypeFunc, { nullable: true })(
        prototype,
        field,
      );  
    } else {
      Field({ nullable: true })(
        prototype,
        field,
      );  
    }

    const metadataStorage = getMetadataStorage();

    metadataStorage.filters.push({
      field,
      // getReturnType: returnTypeFunction,
      target: prototype.constructor,
    });
    metadataStorage.mutateOption.push({
      field,
      target: prototype.constructor,
      options: {
        readOnly: true,
      }
    });
    if (options.sql) {
      const typeMetadata = metadataStorage.fieldLogic.find(l => l.target === prototype.constructor)
      if (typeMetadata) {
        typeMetadata.sqlByField[field] = options.sql
      } else {
        metadataStorage.fieldLogic.push({
          target: prototype.constructor,
          sqlByField: {
            [field]: options.sql,
          }
        });
      }
    }
  };
}
