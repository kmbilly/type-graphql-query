import { ObjectType } from "type-graphql";
import { Entity, Unique } from "typeorm";
import { ModelOptions } from "../interfaces/ModelOptions.interface";
import { getMetadataStorage } from "../metadata";

export function Model(
  options?: ModelOptions,
): ClassDecorator {

  return (target:any) => {
    const metadataStorage = getMetadataStorage();

    Entity()(target)
    const uniqueFields = metadataStorage.uniqueFields
      .filter(u => u.target === target.prototype.constructor)
      .map(u => u.field)
    if (uniqueFields.length > 0) {
      Unique(uniqueFields)(target)
    }

    if (options && options.access) {
      metadataStorage.accesses.push({
        target,
        access: options?.access
      });
    }

    ObjectType()(target)
  };
}
