import { Arg, Field, FieldOptions } from "type-graphql";
import { getMetadataStorage } from "../metadata";
import { ReturnTypeFunc } from "type-graphql/dist/decorators/types";
// import { addSchemaBuilder } from "../helpers";
import { PagingArgs, SortingArgs } from "../types";
import { FilterArg } from "./FilterArg";
// import { SortingArgs } from "../types/connection/SortingArgs";
// import { PagingArgs } from "../types";

/**
 * This decorator will store filters information for the field in a metadata storage.
 * We will use this metadata later on to generate an InputType for the filters argument
 *
 * @param returnTypeFunction
 * @param options
 */
export function FilterableField(): PropertyDecorator;
export function FilterableField(options: FieldOptions): PropertyDecorator;
export function FilterableField(
  returnTypeFunction?: ReturnTypeFunc,
  options?: FieldOptions,
): PropertyDecorator;
export function FilterableField(
  returnTypeFuncOrOptions?: ReturnTypeFunc | FieldOptions,
  maybeOptions?: FieldOptions,
  connection?: boolean,
): PropertyDecorator {
  return (prototype:any, field: string | symbol) => {
    // let returnTypeFunction;

    let newField: string | symbol
    if (typeof returnTypeFuncOrOptions === "function") {
      const returnTypeFunc:any = returnTypeFuncOrOptions as ReturnTypeFunc
      if (connection) {
        newField = field as string

        // return type is a connection, override class property as a function with filter / paging inputs
        Reflect.defineMetadata("design:paramtypes", [String,String,String], prototype, newField)
        prototype[newField] = (
          filter:any,
          paging:any = new PagingArgs(),
          sorting:any,
        ) => {}

        // FilterArg(returnTypeFunc)(
        FilterArg(() => returnTypeFunc().baseType())(
            prototype, newField, 0
        )
        Arg('paging', () => PagingArgs, { nullable: true })(
          prototype, newField, 1
        )
        Arg('sorting', () => [SortingArgs], { nullable: true })(
          prototype, newField, 2
        )

        Field(returnTypeFunc, maybeOptions)(
          prototype,
          newField,
        );
        // returnTypeFunction = () => [returnTypeFunc().baseType()]
      } else {
        newField = field;
        Field(returnTypeFuncOrOptions as ReturnTypeFunc, maybeOptions)(
          prototype,
          field,
        );  
        // returnTypeFunction = returnTypeFuncOrOptions as ReturnTypeFunc
      }

    } else {
      newField = field;
      Field(returnTypeFuncOrOptions as FieldOptions)(
        prototype,
        field,
      );  
    }

    // FiltableField extends Field
    // if (typeof returnTypeFuncOrOptions === "function") {
      // Field(returnTypeFuncOrOptions as ReturnTypeFunc, maybeOptions)(
      //   prototype,
      //   field,
      // );
    //   returnTypeFunction = returnTypeFuncOrOptions as ReturnTypeFunc
    // } else {
      // Field(returnTypeFuncOrOptions as FieldOptions)(
      //   prototype,
      //   field,
      // );
    // }
    const metadataStorage = getMetadataStorage();

    metadataStorage.filters.push({
      field: newField,
      // getReturnType: returnTypeFunction,
      target: prototype.constructor,
    });
  };
}
