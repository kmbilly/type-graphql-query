import { Arg } from "type-graphql";
import { addSchemaBuilder, generateIdType } from "../helpers";

/**
 * This decorator add a filter arg based on the type specified
 * would contains all fields decorated with FillableField
 *
 * @param type
 */
export function IdArg<Entity>(
  returnTypeFunc: Function
): ParameterDecorator {
  return (prototype, propertyKey, parameterIndex) => {
    const getBuildResult = addSchemaBuilder(generateIdType(returnTypeFunc))
    Arg('id', getBuildResult)(
      prototype, propertyKey, parameterIndex
    );
  };
}
