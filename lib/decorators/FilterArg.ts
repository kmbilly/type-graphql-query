import { Arg } from "type-graphql";
import { addSchemaBuilder, generateFilterType } from "../helpers";

/**
 * This decorator add a filter arg based on the type specified
 * would contains all fields decorated with FillableField
 *
 * @param type
 */
export function FilterArg(
  typeFunction: Function
): ParameterDecorator {
  return (prototype, propertyKey, parameterIndex) => {
    const getBuildResult = addSchemaBuilder(generateFilterType(typeFunction))
    Arg('filter', getBuildResult, { nullable: true })(
      prototype, propertyKey, parameterIndex
    );
  };
}
