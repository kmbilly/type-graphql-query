import { GraphQLScalarType, Kind } from "graphql";

export const DateOnly = new GraphQLScalarType({
  name: "DateOnly",
  description: "Date only scalar type with format YYYY-MM-DD",
  parseValue(value: string) {
    // value from the client input variables
    return new Date(value); 
  },
  serialize(value: Date | string) {
    let result:string = value as string
    if (typeof value === 'object') {
      result =
        value.getFullYear() + '-' +
        (value.getMonth() + 1).toString().padStart(2, '0') + '-' +
        value.getDate().toString().padStart(2, '0')
    }
    // value sent to the client
    return result; 
  },
  parseLiteral(ast) {
    // value from the client query
    if (ast.kind === Kind.STRING) {
      return new Date(ast.value); 
    }
    return null;
  },
});