import { GraphQLScalarType, Kind } from "graphql";

export const Decimal = new GraphQLScalarType({
  name: "Decimal",
  description: "Decimal type without precision loss",
  parseValue(value: string) {
    // value from the client input variables
    return value; 
  },
  serialize(value: string) {
    // value sent to the client
    return value; 
  },
  parseLiteral(ast) {
    // value from the client query
    if (ast.kind === Kind.STRING) {
      return ast.value; 
    }
    return null;
  },
});