import { Field, Int, ObjectType } from 'type-graphql'

@ObjectType()
export class ListStat {
  @Field(type => Int)
  totalCount?: number
}
