import { ClassType, Field, InputType } from "type-graphql"

export function ArrayComparison<TItem>(TItemClass: ClassType<TItem>):any {
  @InputType({ isAbstract: true })
  abstract class ArrayComparisonClass {
    @Field(type => TItemClass, { nullable: true })
    every: TItem

    @Field(type => TItemClass, { nullable: true })
    some: TItem

    @Field(type => TItemClass, { nullable: true })
    none: TItem

    @Field({ nullable: true })
    isNull: boolean
  }

  return ArrayComparisonClass
}
