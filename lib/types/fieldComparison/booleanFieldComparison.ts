import { Field, InputType } from "type-graphql";
import { IsBoolean, IsOptional } from 'class-validator';

@InputType()
export class BooleanFieldComparison {
  @Field(() => Boolean, { nullable: true })
  @IsBoolean()
  @IsOptional()
  is?: boolean | null;

  @Field(() => Boolean, { nullable: true })
  @IsBoolean()
  @IsOptional()
  isNot?: boolean | null;
}