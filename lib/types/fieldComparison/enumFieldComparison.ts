import { Field, InputType } from "type-graphql";
import { IsBoolean, IsString, IsOptional } from 'class-validator';
import { IsUndefined } from '../validators';

@InputType()
export class EnumFieldComparison {
  @Field(() => Boolean, { nullable: true })
  @IsBoolean()
  @IsOptional()
  is?: boolean | null;

  @Field(() => Boolean, { nullable: true })
  @IsBoolean()
  @IsOptional()
  isNot?: boolean | null;

  @Field({ nullable: true })
  @IsString()
  @IsUndefined()
  eq?: string;

  @Field({ nullable: true })
  @IsString()
  @IsUndefined()
  neq?: string;

  @Field({ nullable: true })
  @IsString()
  @IsUndefined()
  gt?: string;

  @Field({ nullable: true })
  @IsString()
  @IsUndefined()
  gte?: string;

  @Field({ nullable: true })
  @IsString()
  @IsUndefined()
  lt?: string;

  @Field({ nullable: true })
  @IsString()
  @IsUndefined()
  lte?: string;

  @Field(() => [String], { nullable: true })
  @IsUndefined()
  @IsString({ each: true })
  in?: string[];

  @Field(() => [String], { nullable: true })
  @IsUndefined()
  @IsString({ each: true })
  notIn?: string[];
}