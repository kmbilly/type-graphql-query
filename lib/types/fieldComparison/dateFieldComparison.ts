import { Field, InputType } from "type-graphql";
import { IsBoolean, IsOptional, IsDate, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { IsUndefined } from '../validators';

@InputType()
class DateFieldComparisonBetween {
  @Field({ nullable: false })
  @IsDate()
  lower!: Date;

  @Field({ nullable: false })
  @IsDate()
  upper!: Date;
}

@InputType('DateFieldComparison')
export class DateFieldComparison {
  @Field(() => Boolean, { nullable: true })
  @IsBoolean()
  @IsOptional()
  is?: boolean | null;

  @Field(() => Boolean, { nullable: true })
  @IsBoolean()
  @IsOptional()
  isNot?: boolean | null;

  @Field({ nullable: true })
  @IsUndefined()
  @IsDate()
  eq?: Date;

  @Field({ nullable: true })
  @IsUndefined()
  @IsDate()
  neq?: Date;

  @Field({ nullable: true })
  @IsUndefined()
  @IsDate()
  gt?: Date;

  @Field({ nullable: true })
  @IsUndefined()
  @IsDate()
  gte?: Date;

  @Field({ nullable: true })
  @IsUndefined()
  @IsDate()
  lt?: Date;

  @Field({ nullable: true })
  @IsUndefined()
  @IsDate()
  lte?: Date;

  @Field(() => [Date], { nullable: true })
  @IsUndefined()
  @IsDate({ each: true })
  in?: Date[];

  @Field(() => [Date], { nullable: true })
  @IsUndefined()
  @IsDate({ each: true })
  notIn?: Date[];

  @Field(() => DateFieldComparisonBetween, { nullable: true })
  @ValidateNested()
  @Type(() => DateFieldComparisonBetween)
  between?: DateFieldComparisonBetween;

  @Field(() => DateFieldComparisonBetween, { nullable: true })
  @ValidateNested()
  @Type(() => DateFieldComparisonBetween)
  notBetween?: DateFieldComparisonBetween;
}