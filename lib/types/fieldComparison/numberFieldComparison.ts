import { Field, InputType } from "type-graphql";
import { IsBoolean, IsOptional, ValidateNested, IsNumber } from 'class-validator';
import { Type } from 'class-transformer';
import { IsUndefined } from '../validators';

@InputType()
class NumberFieldComparisonBetween {
  @Field({ nullable: false })
  @IsNumber()
  lower!: number;

  @Field({ nullable: false })
  @IsNumber()
  upper!: number;
}

@InputType()
export class NumberFieldComparison {
  @Field(() => Boolean, { nullable: true })
  @IsBoolean()
  @IsOptional()
  is?: boolean | null;

  @Field(() => Boolean, { nullable: true })
  @IsBoolean()
  @IsOptional()
  isNot?: boolean | null;

  @Field({ nullable: true })
  @IsNumber()
  @IsUndefined()
  eq?: number;

  @Field({ nullable: true })
  @IsNumber()
  @IsUndefined()
  neq?: number;

  @Field({ nullable: true })
  @IsNumber()
  @IsUndefined()
  gt?: number;

  @Field({ nullable: true })
  @IsNumber()
  @IsUndefined()
  gte?: number;

  @Field({ nullable: true })
  @IsNumber()
  @IsUndefined()
  lt?: number;

  @Field({ nullable: true })
  @IsNumber()
  @IsUndefined()
  lte?: number;

  @Field(() => [Number], { nullable: true })
  @IsNumber({}, { each: true })
  @IsUndefined()
  in?: number[];

  @Field(() => [Number], { nullable: true })
  @IsNumber({}, { each: true })
  @IsUndefined()
  notIn?: number[];

  @Field(() => NumberFieldComparisonBetween, { nullable: true })
  @ValidateNested()
  @Type(() => NumberFieldComparisonBetween)
  between?: NumberFieldComparisonBetween;

  @Field(() => NumberFieldComparisonBetween, { nullable: true })
  @ValidateNested()
  @Type(() => NumberFieldComparisonBetween)
  notBetween?: NumberFieldComparisonBetween;
}