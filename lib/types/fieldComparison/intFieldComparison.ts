import { Field, InputType, Int } from "type-graphql";
import { IsBoolean, IsOptional, ValidateNested, IsInt } from 'class-validator';
import { Type } from 'class-transformer';
import { IsUndefined } from '../validators';

@InputType()
class IntFieldComparisonBetween {
  @Field(() => Int, { nullable: false })
  @IsInt()
  lower!: number;

  @Field(() => Int, { nullable: false })
  @IsInt()
  upper!: number;
}

@InputType()
export class IntFieldComparison {
  @Field(() => Boolean, { nullable: true })
  @IsBoolean()
  @IsOptional()
  is?: boolean | null;

  @Field(() => Boolean, { nullable: true })
  @IsBoolean()
  @IsOptional()
  isNot?: boolean | null;

  @Field(() => Int, { nullable: true })
  @IsInt()
  @IsUndefined()
  eq?: number;

  @Field(() => Int, { nullable: true })
  @IsInt()
  @IsUndefined()
  neq?: number;

  @Field(() => Int, { nullable: true })
  @IsInt()
  @IsUndefined()
  gt?: number;

  @Field(() => Int, { nullable: true })
  @IsInt()
  @IsUndefined()
  gte?: number;

  @Field(() => Int, { nullable: true })
  @IsInt()
  @IsUndefined()
  lt?: number;

  @Field(() => Int, { nullable: true })
  @IsInt()
  @IsUndefined()
  lte?: number;

  @Field(() => [Int], { nullable: true })
  @IsInt({ each: true })
  @IsUndefined()
  in?: number[];

  @Field(() => [Int], { nullable: true })
  @IsInt({ each: true })
  @IsUndefined()
  notIn?: number[];

  @Field(() => IntFieldComparisonBetween, { nullable: true })
  @ValidateNested()
  @Type(() => IntFieldComparisonBetween)
  between?: IntFieldComparisonBetween;

  @Field(() => IntFieldComparisonBetween, { nullable: true })
  @ValidateNested()
  @Type(() => IntFieldComparisonBetween)
  notBetween?: IntFieldComparisonBetween;
}
