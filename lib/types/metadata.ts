// import { GraphQLScalarType } from "graphql";

// export type ReturnTypeFunc = (type?: void) => GraphQLScalarType | Function;
// import { ReturnTypeFunc } from "type-graphql/dist/decorators/types";
import { MutateOptionOptions } from "../decorators/MutateOption";

export type FiltersCollectionType = {
  target: Function;
  field: string | symbol;
  // getReturnType?: ReturnTypeFunc;
};

export type MutateOptionCollectionType = {
  target: Function;
  field: string | symbol;
  options: MutateOptionOptions
};

export type FieldLogicCollectionType = {
  target: Function;
  sqlByField: any;
};

export type UniqueFieldsCollectionType = {
  target: Function;
  field: string;
};

export type ModelAccessType = {
  target: Function;
  access: {
    get: string | string[],
    create: string | string[],
    update: string | string[],
    delete: string | string[],
  };
}

// export type RelationsCollectionType = {
//   target: Function;
//   field: string | symbol;
//   type: ObjectType<any>;
//   toMany?: boolean;
//   toOne?: boolean;
// };

export type MetadataStorage = {
  filters: FiltersCollectionType[];
  mutateOption: MutateOptionCollectionType[];
  fieldLogic: FieldLogicCollectionType[];
  uniqueFields: UniqueFieldsCollectionType[];
  accesses: ModelAccessType[];
  // relations: RelationsCollectionType[];
};
