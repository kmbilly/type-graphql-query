import { ObjectType, Field, Int } from 'type-graphql'

@ObjectType()
export class PageInfo {
  @Field(type => Int)
  offset: number

  @Field(type => Int)
  limit: number

  @Field(type => Int, { nullable: true })
  totalCount?: number
}
