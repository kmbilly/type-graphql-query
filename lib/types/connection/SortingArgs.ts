import { InputType, Field, registerEnumType } from 'type-graphql'
import { SortDirection, SortField } from '../../interfaces';

// /**
//  * Enum for sorting either ASC or DESC.
//  */
// export enum SortDirection {
//   ASC = 'ASC',
//   DESC = 'DESC',
// }

registerEnumType(SortDirection, {
  name: "SortDirection",
});

@InputType()
export class SortingArgs<T> implements SortField<T> {
  @Field(type => String)
  field: keyof T;

  @Field(type => SortDirection)
  direction: SortDirection
}
