import { ObjectType, Field } from 'type-graphql'
// import { addSchemaBuilder } from '../../helpers'
import { PageInfo } from './PageInfo'

const generatedClasses:any = {}

// function Connection<TItem>(TItemClass: ClassType<TItem>):any {
function Connection<TItem>(TItemClassFunc: any):any {
  @ObjectType({ isAbstract: true })
  abstract class ConnectionClass {
    @Field()
    pageInfo: PageInfo

    @Field(type => [TItemClassFunc()])
    nodes?: TItem[]

    static baseType() {
      return TItemClassFunc()
    }
  }

  return ConnectionClass
}

// function returnConnectionClass<TItem>(name: string, TItemClass: ClassType<TItem>):any {
function returnConnectionClass(name: string, TItemClassFunc: any):any {
  if (!generatedClasses[name]) {
    const connectionName = `${name}Connection`
    const connectionContainer = {
      [connectionName]: class extends Connection(TItemClassFunc){},
    };
    ObjectType()(connectionContainer[connectionName])

    generatedClasses[name] = connectionContainer[connectionName]
  }

  return () => generatedClasses[name]
}

export { Connection, returnConnectionClass }