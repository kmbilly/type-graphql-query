import { InputType, Field, Int } from 'type-graphql'
import { Min, Max } from "class-validator";

@InputType()
export class PagingArgs {
  @Field(type => Int, { defaultValue: 0 })
  @Min(0)
  offset: number = 0

  @Field(type => Int, { defaultValue: 10 })
  @Min(1)
  @Max(100)
  limit: number = 10
}
