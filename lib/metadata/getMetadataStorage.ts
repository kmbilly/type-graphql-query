import { MetadataStorage } from "../types";

const metadataStorage = {
  filters: [],
  mutateOption: [],
  fieldLogic: [],
  uniqueFields: [],
  accesses: [],
  // relations: [],
};

export function getMetadataStorage(): MetadataStorage {
  return metadataStorage;
}
